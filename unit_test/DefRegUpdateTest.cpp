/* 
 * Copyright (C) 2006-2015 Registro.br. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistribution of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY REGISTRO.BR ``AS IS AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIE OF FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL REGISTRO.BR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 */
/* $Id$ */

#include <memory>
#include <string>

#include "libepp_nicbr.H"

#include "DefRegUpdateTest.H"
#include "DefRegUpdate.H"
#include "FileUtil.H"
#include "IoException.H"
#include "XmlException.H"

using std::auto_ptr;

LIBEPP_NICBR_NS_USE

CPPUNIT_TEST_SUITE_REGISTRATION(DefRegUpdateTest);

DefRegUpdateTest::DefRegUpdateTest() {}

DefRegUpdateTest::~DefRegUpdateTest() {}

void DefRegUpdateTest::setUp() {}

void DefRegUpdateTest::tearDown() {}

void DefRegUpdateTest::set_xml_template_test()
{
	string to_be_parsed =
		"<command>"
		"<update>"
		"<defReg:update "
		"xmlns:defReg=\"http://nic.br/epp/defReg-1.0\" "
		"xsi:schemaLocation=\"http://nic.br/epp/defReg-1.0 "
		"defReg-1.0.xsd\">"
		"<defReg:roid>$(roid)$</defReg:roid>"
		"$(add)$"
		"$(rem)$"
		"$(chg)$"
		"</defReg:update>"
		"</update>"
		"$(clTRID)$"
		"</command>";

	DefRegUpdate def_reg_update;
	DefRegUpdateCmd* def_reg_update_cmd = def_reg_update.get_command();
	def_reg_update_cmd->set_roid("EXAMPLE1-REP");
	def_reg_update_cmd->insert_status_add
		(DefRegUpdateCmd::Status("clientDeleteProhibited", "en", "Deletions not desired."));
	def_reg_update_cmd->insert_status_rem
		(DefRegUpdateCmd::Status("clientUpdateProhibited"));
	def_reg_update_cmd->set_registrant("sh8013");
	def_reg_update_cmd->set_trademark_id("XYZ-123");
	def_reg_update_cmd->set_trademark_country("US");
	def_reg_update_cmd->set_trademark_date("1990-04-03");
	def_reg_update_cmd->set_admin_contact("sh8013");

	AuthInfo auth;
	auth.set_roid("SH8013-REP");
	auth.set_pw("abc123");
	def_reg_update_cmd->set_authInfo(auth);

	def_reg_update.get_command()->set_clTRID("ABC-12345");
	def_reg_update.set_xml_template(to_be_parsed);

	string expected =
		"<command>"
		"<update>"
		"<defReg:update xmlns:defReg=\"http://nic.br/epp/defReg-1.0\" "
		"xsi:schemaLocation=\"http://nic.br/epp/defReg-1.0 "
		"defReg-1.0.xsd\">"
		"<defReg:roid>EXAMPLE1-REP</defReg:roid>"
		"<defReg:add>"
		"<defReg:status s=\"clientDeleteProhibited\" lang=\"en\">"
		"Deletions not desired."
		"</defReg:status>"
		"</defReg:add>"
		"<defReg:rem>"
		"<defReg:status s=\"clientUpdateProhibited\"/>"
		"</defReg:rem>"
		"<defReg:chg>"
		"<defReg:registrant>sh8013</defReg:registrant>"
		"<defReg:tm>XYZ-123</defReg:tm>"
		"<defReg:tmCountry>US</defReg:tmCountry>"
		"<defReg:tmDate>1990-04-03</defReg:tmDate>"
		"<defReg:adminContact>sh8013</defReg:adminContact>"
		"<defReg:authInfo>"
		"<defReg:pw roid=\"SH8013-REP\">abc123</defReg:pw>"
		"</defReg:authInfo>"
		"</defReg:chg>"
		"</defReg:update>"
		"</update>"
		"<clTRID>ABC-12345</clTRID>"
		"</command>";

	CPPUNIT_ASSERT_EQUAL(expected, def_reg_update.get_xml());

	// tests reset method
	def_reg_update_cmd->reset();
	CPPUNIT_ASSERT(def_reg_update_cmd->get_roid().empty());
	CPPUNIT_ASSERT(def_reg_update_cmd->get_status_add().empty());
	CPPUNIT_ASSERT(def_reg_update_cmd->get_status_rem().empty());
	CPPUNIT_ASSERT(def_reg_update_cmd->get_registrant().empty());
	CPPUNIT_ASSERT(def_reg_update_cmd->get_trademark_id().empty());
	CPPUNIT_ASSERT(def_reg_update_cmd->get_trademark_country().empty());
	CPPUNIT_ASSERT(def_reg_update_cmd->get_trademark_date().empty());
	CPPUNIT_ASSERT(def_reg_update_cmd->get_admin_contact().empty());
	CPPUNIT_ASSERT(def_reg_update_cmd->get_authInfo().get_pw().empty());
	CPPUNIT_ASSERT(def_reg_update_cmd->get_authInfo().get_roid().empty());

	// parent attribute
	CPPUNIT_ASSERT(def_reg_update_cmd->get_clTRID().empty());
}

void DefRegUpdateTest::command_test()
{
	DefRegUpdate def_reg_update;
	DefRegUpdateCmd* def_reg_update_cmd = def_reg_update.get_command();
	def_reg_update_cmd->set_roid("EXAMPLE1-REP");
	def_reg_update_cmd->insert_status_add
		(DefRegUpdateCmd::Status("clientDeleteProhibited", "en", "Deletions not desired."));
	def_reg_update_cmd->insert_status_rem
		(DefRegUpdateCmd::Status("clientUpdateProhibited"));
	def_reg_update_cmd->set_registrant("sh8013");
	def_reg_update_cmd->set_trademark_id("XYZ-123");
	def_reg_update_cmd->set_trademark_country("US");
	def_reg_update_cmd->set_trademark_date("1990-04-03");
	def_reg_update_cmd->set_admin_contact("sh8013");

	AuthInfo auth;
	auth.set_roid("SH8013-REP");
	auth.set_pw("abc123");
	def_reg_update_cmd->set_authInfo(auth);

	bool exception_caught = false;
	try {
		string xml_template =
			FileUtil::read_file("../docs/templates/def_reg_update.xml");

		def_reg_update.get_command()->set_clTRID("ABC-12345");
		def_reg_update.set_xml_template(xml_template);

		DomParser parser;
		parser.enable_validation("../docs/schemas");
		parser.parse_command(def_reg_update.get_xml());
	} catch (const IoException &e) {
		exception_caught = true;
		printf("\nIO Exception: code [%d] message [%s]",
		       e.get_code(), e.get_msg().c_str());
	} catch (const XmlException &e) {
		exception_caught = true;
		printf("\nXml Exception: code [%d] message [%s] low level message [%s]\n",
		       e.get_code(), e.get_msg().c_str(), e.get_low_level_msg().c_str());
	}

	CPPUNIT_ASSERT(!exception_caught);
}

void DefRegUpdateTest::response_test()
{
	string expected =
		"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>"
		"<epp xmlns=\"urn:ietf:params:xml:ns:epp-1.0\" "
		"xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "
		"xsi:schemaLocation=\"urn:ietf:params:xml:ns:epp-1.0 epp-1.0.xsd\">"
		"<response>"
		"<result code=\"1000\">"
		"<msg>Command completed successfully</msg>"
		"</result>"
		"<trID>"
		"<clTRID>ABC-12345</clTRID>"
		"<svTRID>54322-XYZ</svTRID>"
		"</trID>"
		"</response>"
		"</epp>";

	DomParser parser;
	parser.enable_validation("../docs/schemas");

	DefRegUpdate def_reg_update;

	CPPUNIT_ASSERT_NO_THROW(def_reg_update.set_response(expected, &parser));
	Response *def_reg_update_rsp = def_reg_update.get_response();

	string result_code = "1000";

	map <Response::ResultCode, Response::ResultInfo> results;
	map <Response::ResultCode, Response::ResultInfo>::const_iterator r_it;
	results = def_reg_update_rsp->get_result_list();
	r_it = results.begin();

	CPPUNIT_ASSERT(r_it != results.end());
	CPPUNIT_ASSERT_EQUAL(Response::OK, r_it->first);

	string response =
		"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>"
		"<epp xmlns=\"urn:ietf:params:xml:ns:epp-1.0\" "
		"xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "
		"xsi:schemaLocation=\"urn:ietf:params:xml:ns:epp-1.0 "
		"epp-1.0.xsd\">"
		"<response>"
		"<result code=\"" + result_code + "\">"
		"<msg";

	string result_lang = def_reg_update_rsp->get_result_lang();
	if (result_lang != "en") {
		response += " lang='" + result_lang + "'";
	}

	response +=
		">" + r_it->second.msg + "</msg>"
		"</result>"
		"<trID>"
		"<clTRID>" + def_reg_update_rsp->get_clTRID() + "</clTRID>"
		"<svTRID>" + def_reg_update_rsp->get_svTRID() + "</svTRID>"
		"</trID>"
		"</response>"
		"</epp>";

	CPPUNIT_ASSERT_EQUAL(expected, response);
}
