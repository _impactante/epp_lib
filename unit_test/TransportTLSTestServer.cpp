/* 
 * Copyright (C) 2006-2015 Registro.br. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistribution of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY REGISTRO.BR ``AS IS AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIE OF FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL REGISTRO.BR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 */
/* $Id: TransportTLSTestServer.cpp 1281 2015-01-28 17:13:38Z rafael $ */
/**
   @file  TransportTLSTestServer.cpp
   @brief Small echo server for Transport testing's sake
*/

#include <openssl/bio.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/rand.h>
#include <openssl/dh.h>

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>

#define   PORT_FILE    "TransportTLSTestServer.port"

const int READ_LIMIT = 1024; /* bytes */
const int TIMEOUT    = 60;   /* seconds */

static void sig_handler(int signo);

// openssl dhparam -5 -check -rand /dev/urandom -C 512
DH *get_dh512()
{
	static unsigned char dh512_p[]={
		0x9E,0xCB,0x35,0xD0,0xCB,0x56,0x46,0x60,0xC3,0xA5,0x6F,0x7A,
		0x74,0xA9,0xBF,0x5E,0xFE,0xC9,0x02,0xA8,0x29,0xFA,0xD0,0x1B,
		0x3E,0xA6,0xF6,0xFB,0x01,0x43,0x8B,0x68,0x9E,0xE6,0x7A,0xF8,
		0x73,0x11,0x5A,0x10,0x49,0x60,0x8B,0x22,0x48,0x01,0xFE,0xA6,
		0xA4,0x65,0xE9,0x54,0x57,0x79,0x3C,0x80,0x12,0x56,0x8F,0xB4,
		0x62,0xCB,0xC3,0x63,
	};
	static unsigned char dh512_g[]={
		0x05,
	};
	DH *dh;

	if ((dh=DH_new()) == NULL) return(NULL);
	dh->p=BN_bin2bn(dh512_p,sizeof(dh512_p),NULL);
	dh->g=BN_bin2bn(dh512_g,sizeof(dh512_g),NULL);
	if ((dh->p == NULL) || (dh->g == NULL))
		{ DH_free(dh); return(NULL); }
	return(dh);
}

// openssl dhparam -5 -check -rand /dev/urandom -C 1024
DH *get_dh1024()
{
	static unsigned char dh1024_p[]={
		0xA9,0xF6,0xA7,0x94,0x29,0xEE,0x09,0x75,0x23,0xF0,0x28,0xD8,
		0x57,0x82,0x1F,0x71,0xDA,0x7E,0xB9,0x47,0x56,0xEE,0x52,0x18,
		0xDC,0x5D,0x68,0xB0,0x09,0x21,0xE5,0x89,0x84,0xFB,0x46,0x6A,
		0xD8,0x59,0xBA,0x51,0x0D,0xD3,0x91,0x55,0xC7,0xD9,0x26,0xC6,
		0x1D,0xA7,0x64,0xE4,0xBF,0xC1,0x9A,0x81,0x4A,0x9A,0x3C,0x96,
		0x44,0x86,0xCC,0x02,0x96,0x3E,0xF6,0x98,0x41,0x46,0x48,0xA5,
		0x42,0x0F,0x0C,0x0C,0x28,0x99,0x1E,0x5A,0x34,0x89,0x28,0x7D,
		0xEC,0xC7,0x67,0xBD,0x81,0x5B,0x7B,0x6C,0xB0,0x49,0xBF,0x04,
		0x7E,0xD9,0x5A,0xA3,0xCA,0x3D,0x0F,0xC6,0xDA,0xDE,0x42,0xC7,
		0xDE,0x82,0xFB,0x14,0x23,0x36,0x65,0x07,0x64,0x99,0x7F,0x21,
		0x00,0x77,0x8A,0x03,0x0F,0x11,0x3F,0x37,
	};
	static unsigned char dh1024_g[]={
		0x05,
	};
	DH *dh;

	if ((dh=DH_new()) == NULL) return(NULL);
	dh->p=BN_bin2bn(dh1024_p,sizeof(dh1024_p),NULL);
	dh->g=BN_bin2bn(dh1024_g,sizeof(dh1024_g),NULL);
	if ((dh->p == NULL) || (dh->g == NULL))
		{ DH_free(dh); return(NULL); }
	return(dh);
}

DH *tmp_dh_callback(SSL *s, int is_export, int keylength)
{
	EVP_PKEY *pkey = SSL_get_privatekey(s);
	int type = pkey ? EVP_PKEY_type(pkey->type) : EVP_PKEY_NONE;

	/* httpd-2.4.10 --> modules/ssl/ssl_engine_kernel.c
	 * :: function ssl_callback_TmpDH (line 1342)
	 *
	 * OpenSSL will call us with either keylen == 512 or keylen == 1024
	 * (see the definition of SSL_EXPORT_PKEYLENGTH in ssl_locl.h).
	 * Adjust the DH parameter length according to the size of the
	 * RSA/DSA private key used for the current connection.
	 */
	if ((type == EVP_PKEY_RSA) || (type == EVP_PKEY_DSA)) {
		keylength = EVP_PKEY_bits(pkey);
	}
	
	DH *dh_tmp=NULL;

	if (keylength < 1024) {
		dh_tmp = get_dh512();
	} else {
		dh_tmp = get_dh1024();
	}

	return dh_tmp;
}

int main(int argc, char **argv)
{
	signal(SIGINT,  sig_handler);
	signal(SIGTERM, sig_handler);
	signal(SIGALRM, sig_handler);

	pid_t pid;
	if ((pid = fork()) < 0) {
		/* fork error */
		fprintf(stderr, "fork error.\n");
		exit(1);
	} else if (pid != 0) {
		/* parent quits */
		exit(0);
	}

	/* child continues */

	SSL_library_init();

	SSL *ssl;
	SSL_CTX *ssl_ctx;

	ssl_ctx = SSL_CTX_new(SSLv23_server_method());

	SSL_CTX_set_tmp_dh_callback(ssl_ctx, tmp_dh_callback);
	SSL_CTX_set_cipher_list(ssl_ctx,
	                        "EECDH:EDH:!RC4:!aNULL:!eNULL:!LOW:!3DES:"
	                        "!MD5:!EXP:!PSK:!SRP:!DSS");

	if (SSL_CTX_load_verify_locations(ssl_ctx, "root.pem", NULL) != 1) {
		fprintf(stderr, "Error loading CA Certificate file.\n");
		exit(1);
	}
	if (SSL_CTX_use_certificate_chain_file(ssl_ctx, "server.pem") != 1) {
		fprintf(stderr, "Error loading certificate from file.\n");
		exit(1);
	}
	if (SSL_CTX_use_PrivateKey_file(ssl_ctx, "server.pem",
	                                SSL_FILETYPE_PEM) != 1) {
		fprintf(stderr, "Error loading private key from file.\n");
		exit(1);
	}
	SSL_CTX_set_verify(ssl_ctx,
	                   SSL_VERIFY_PEER|
	                   SSL_VERIFY_FAIL_IF_NO_PEER_CERT|
	                   SSL_VERIFY_CLIENT_ONCE,
	                   NULL);
	SSL_CTX_set_verify_depth(ssl_ctx, 3);
	SSL_CTX_set_options(ssl_ctx,
	                    SSL_OP_NO_SSLv2|
	                    SSL_OP_NO_SSLv3|
	                    SSL_OP_SINGLE_DH_USE);
	
	/* finds an available port >= 1024 */
	BIO *acc = 0;
	char port[6], address_port[8];
	int i;
	for (i = 1024; i <= 65535; ++i) {
		BIO_free(acc);

		sprintf(address_port, "*:%d", i);
		if ((acc = BIO_new_accept(address_port)) <= 0) {
			/* error creating server socket */
			if (i == 65535) {
				exit(1);
			}
			continue;
		}
		sprintf(port, "%d", i);
  
		if (BIO_do_accept(acc) <= 0) {
			/* error binding server socket */
			if (i == 65535) {
				exit(1);
			}
			continue;
		}

		break;
	}

	/* write port number to tmp file */
	FILE *port_fd = fopen(PORT_FILE, "w");
	if (!port_fd) {
		exit(1);
	}
	int written = fprintf(port_fd, "%s", port);
	if (written < 0 || (size_t) written < strlen(port)) {
		unlink(PORT_FILE);
		exit(1);
	}
	fclose(port_fd);

	/* timeout */
	alarm(TIMEOUT);

	/* awaits connection */
	if (BIO_do_accept(acc) <= 0) {
		/* error accepting connection */
		unlink(PORT_FILE);
		exit(1);
	}

	BIO *client = 0;
	client = BIO_pop(acc);

	if (!(ssl = SSL_new(ssl_ctx))) {
		/* error creating an SSL context */
		unlink(PORT_FILE);
		exit(1);
	}
	SSL_set_bio(ssl, client, client);

	if (SSL_accept(ssl) <= 0) {
		/* error accepting SSL connection */
		unlink(PORT_FILE);
		exit(1);
	}

	/* timeout off */
	alarm(0);

	/* reads something from client */
	int err;
	char buf[READ_LIMIT];
	err = SSL_read(ssl, buf, sizeof(buf));
	/* writes it back to client */
	err = SSL_write(ssl, buf, err);

	if (err <= 0) {
		/* error writing to socket */
		unlink(PORT_FILE);
		exit(1);
	}
  
	/* clean up */
	SSL_free(ssl);
	SSL_CTX_free(ssl_ctx);
	BIO_free(acc);
	unlink(PORT_FILE);

	return 0;
}

static void sig_handler(int signo)
{
	unlink(PORT_FILE);
	exit(1);
}
