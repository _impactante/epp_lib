/* 
 * Copyright (C) 2006-2015 Registro.br. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistribution of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY REGISTRO.BR ``AS IS AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIE OF FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL REGISTRO.BR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 */
/* $Id$ */

#include <memory>
#include <string>

#include "libepp_nicbr.H"

#include "DefRegCheckTest.H"
#include "DefRegCheck.H"
#include "FileUtil.H"
#include "IoException.H"
#include "XmlException.H"

using std::auto_ptr;

LIBEPP_NICBR_NS_USE

CPPUNIT_TEST_SUITE_REGISTRATION(DefRegCheckTest);

DefRegCheckTest::DefRegCheckTest() {}

DefRegCheckTest::~DefRegCheckTest() {}

void DefRegCheckTest::setUp() {}

void DefRegCheckTest::tearDown() {}

void DefRegCheckTest::set_xml_template_test()
{
	string to_be_parsed =
		"<command>"
		"<check>"
		"<defReg:check "
		"xmlns:defReg=\"http://nic.br/epp/defReg-1.0\" "
		"xsi:schemaLocation=\"http://nic.br/epp/defReg-1.0 "
		"defReg-1.0.xsd\">"
		"$(name_list)$"
		"</defReg:check>"
		"</check>"
		"$(clTRID)$"
		"</command>";

	DefRegCheck def_reg_check;
	DefRegCheckCmd* def_reg_check_cmd = def_reg_check.get_command();
	def_reg_check_cmd->insert_name(DefRegName("doe", DefRegLevel::PREMIUM));
	def_reg_check_cmd->insert_name(DefRegName("john.doe", DefRegLevel::STANDARD));

	def_reg_check.get_command()->set_clTRID("ABC-12345");
	def_reg_check.set_xml_template(to_be_parsed);

	string expected =
		"<command>"
		"<check>"
		"<defReg:check "
		"xmlns:defReg=\"http://nic.br/epp/defReg-1.0\" "
		"xsi:schemaLocation=\"http://nic.br/epp/defReg-1.0 "
		"defReg-1.0.xsd\">"
		"<defReg:name level=\"premium\">doe</defReg:name>"
		"<defReg:name level=\"standard\">john.doe</defReg:name>"
		"</defReg:check>"
		"</check>"
		"<clTRID>ABC-12345</clTRID>"
		"</command>";

	CPPUNIT_ASSERT_EQUAL(expected, def_reg_check.get_xml());

	// tests reset method
	def_reg_check_cmd->reset();
	CPPUNIT_ASSERT(def_reg_check_cmd->get_name_list().empty());

	// parent attribute
	CPPUNIT_ASSERT(def_reg_check_cmd->get_clTRID() == "");
}

void DefRegCheckTest::command_test()
{
	DefRegCheck def_reg_check;
	DefRegCheckCmd *def_reg_check_cmd = def_reg_check.get_command();

	def_reg_check_cmd->insert_name(DefRegName("doe", DefRegLevel::PREMIUM));
	def_reg_check_cmd->insert_name(DefRegName("john.doe", DefRegLevel::STANDARD));

	bool exception_caught = false;
	try {
		string xml_template =
			FileUtil::read_file("../docs/templates/def_reg_check.xml");

		def_reg_check.get_command()->set_clTRID("ABC-12345");
		def_reg_check.set_xml_template(xml_template);

		DomParser parser;
		parser.enable_validation("../docs/schemas");
		parser.parse_command(def_reg_check.get_xml());
	} catch (const IoException &e) {
		exception_caught = true;
		printf("\nIO Exception: code [%d] message [%s]",
		       e.get_code(), e.get_msg().c_str());
	} catch (const XmlException &e) {
		exception_caught = true;
		printf("\nXml Exception: code [%d] message [%s] low level message [%s]\n",
		       e.get_code(), e.get_msg().c_str(), e.get_low_level_msg().c_str());
	}

	CPPUNIT_ASSERT(!exception_caught);
}

void DefRegCheckTest::response_test()
{
	string expected =
		"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>"
		"<epp xmlns=\"urn:ietf:params:xml:ns:epp-1.0\" "
		"xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "
		"xsi:schemaLocation=\"urn:ietf:params:xml:ns:epp-1.0 epp-1.0.xsd\">"
		"<response>"
		"<result code=\"1000\">"
		"<msg>Command completed successfully</msg>"
		"</result>"
		"<resData>"
		"<defReg:chkData "
		"xmlns:defReg=\"http://nic.br/epp/defReg-1.0\" "
		"xsi:schemaLocation=\"http://nic.br/epp/defReg-1.0 defReg-1.0.xsd\">"
		"<defReg:cd>"
		"<defReg:name level=\"premium\" "
		"avail=\"1\">doe</defReg:name>"
		"</defReg:cd>"
		"<defReg:cd>"
		"<defReg:name level=\"standard\" "
		"avail=\"0\">john.doe</defReg:name>"
		"<defReg:reason>Conflicting object "
		"exists</defReg:reason>"
		"</defReg:cd>"
		"</defReg:chkData>"
		"</resData>"
		"<trID>"
		"<clTRID>ABC-12345</clTRID>"
		"<svTRID>54322-XYZ</svTRID>"
		"</trID>"
		"</response>"
		"</epp>";

	DomParser parser;
	parser.enable_validation("../docs/schemas");

	DefRegCheck def_reg_check;

	CPPUNIT_ASSERT_NO_THROW(def_reg_check.set_response(expected, &parser));
	DefRegCheckRsp* def_reg_check_rsp = def_reg_check.get_response();

	string result_code = "1000";

	map <Response::ResultCode, Response::ResultInfo> results;
	map <Response::ResultCode, Response::ResultInfo>::const_iterator r_it;
	results = def_reg_check_rsp->get_result_list();
	r_it = results.begin();

	CPPUNIT_ASSERT(r_it != results.end());
	CPPUNIT_ASSERT_EQUAL(Response::OK, r_it->first);

	string response =
		"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>"
		"<epp xmlns=\"urn:ietf:params:xml:ns:epp-1.0\" "
		"xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "
		"xsi:schemaLocation=\"urn:ietf:params:xml:ns:epp-1.0 "
		"epp-1.0.xsd\">"
		"<response>"
		"<result code=\"" + result_code + "\">"
		"<msg";

	string result_lang = def_reg_check_rsp->get_result_lang();
	if (result_lang != "en") {
		response += " lang='" + result_lang + "'";
	}

	response +=
		">" + r_it->second.msg + "</msg>"
		"</result>"
		"<resData>"
		"<defReg:chkData "
		"xmlns:defReg=\"http://nic.br/epp/defReg-1.0\" "
		"xsi:schemaLocation=\"http://nic.br/epp/defReg-1.0 "
		"defReg-1.0.xsd\">";

	list<DefRegCheckRsp::Name> avail = def_reg_check_rsp->get_names();
	list<DefRegCheckRsp::Name>::const_iterator it;
	for (it = avail.begin(); it != avail.end(); it++) {
		string avail = it->is_avail() ? "1" : "0";
		string level = "";

		if (it->get_level() != DefRegLevel::NONE) {
			level += " level=\"" + DefRegLevel::toStr(it->get_level()) + "\"";
		}
		response += "<defReg:cd><defReg:name" + level + " avail=\"" + avail + "\">" +
			it->get_name() + "</defReg:name>";
		if (strcmp(it->get_reason().c_str(), "") != 0) {
			response += "<defReg:reason>" + it->get_reason() + "</defReg:reason>";
		}
		response += "</defReg:cd>";
	}
	response += "</defReg:chkData>"
		"</resData>"
		"<trID>"
		"<clTRID>" + def_reg_check_rsp->get_clTRID() + "</clTRID>"
		"<svTRID>" + def_reg_check_rsp->get_svTRID() + "</svTRID>"
		"</trID>"
		"</response>"
		"</epp>";

	CPPUNIT_ASSERT_EQUAL(expected, response);

	// tests reset method
	CPPUNIT_ASSERT(!def_reg_check_rsp->get_names().empty());
	def_reg_check_rsp->reset();
	CPPUNIT_ASSERT(def_reg_check_rsp->get_names().empty());
}
