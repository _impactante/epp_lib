/* 
 * Copyright (C) 2006-2015 Registro.br. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistribution of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY REGISTRO.BR ``AS IS AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIE OF FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL REGISTRO.BR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 */
/* $Id$ */

#if USE_BR_ORG
#include <memory>
#include <string>

#include "libepp_nicbr.H"

#include "BrOrgDeleteTest.H"
#include "BrOrgDelete.H"
#include "FileUtil.H"
#include "IoException.H"
#include "XmlException.H"

LIBEPP_NICBR_NS_USE

CPPUNIT_TEST_SUITE_REGISTRATION(BrOrgDeleteTest);

BrOrgDeleteTest::BrOrgDeleteTest() {}

BrOrgDeleteTest::~BrOrgDeleteTest() {}

void BrOrgDeleteTest::setUp() {}

void BrOrgDeleteTest::tearDown() {}

void BrOrgDeleteTest::set_xml_template_test()
{
	string to_be_parsed = "<command>"
		"<delete>"
		"<contact:delete"
		"xmlns:contact='urn:ietf:params:xml:ns:contact-1.0'"
		"xsi:schemaLocation='urn:ietf:params:xml:ns:contact-1.0"
		"contact-1.0.xsd'>"
		"<contact:id>$(id)$</contact:id>"
		"</contact:delete>"
		"</delete>"
		"<extension>"
		"<brorg:delete "
		"xmlns:brorg='urn:ietf:params:xml:ns:brorg-1.0' "
		"xsi:schemaLocation='urn:ietf:params:xml:ns:brorg-1.0 "
		"brorg-1.0.xsd'>"
		"<brorg:organization>"
		"$(organization)$"
		"</brorg:organization>"
		"</brorg:delete>"
		"</extension>"
		"$(clTRID)$"
		"</command>";

	BrOrgDelete br_org_delete;
	BrOrgDeleteCmd *br_org_delete_cmd = br_org_delete.get_command(); 

	br_org_delete_cmd->set_id("e654321");
	br_org_delete_cmd->set_organization("005.506.560/0001-36");

	br_org_delete.get_command()->set_clTRID("ABC-12345");
	br_org_delete.set_xml_template(to_be_parsed);

	string expected = "<command>"
		"<delete>"
		"<contact:delete"
		"xmlns:contact='urn:ietf:params:xml:ns:contact-1.0'"
		"xsi:schemaLocation='urn:ietf:params:xml:ns:contact-1.0"
		"contact-1.0.xsd'>"
		"<contact:id>e654321</contact:id>"
		"</contact:delete>"
		"</delete>"
		"<extension>"
		"<brorg:delete "
		"xmlns:brorg='urn:ietf:params:xml:ns:brorg-1.0' "
		"xsi:schemaLocation='urn:ietf:params:xml:ns:brorg-1.0 "
		"brorg-1.0.xsd'>"
		"<brorg:organization>"
		"005.506.560/0001-36"
		"</brorg:organization>"
		"</brorg:delete>"
		"</extension>"
		"<clTRID>ABC-12345</clTRID>"
		"</command>";

	CPPUNIT_ASSERT(expected == br_org_delete.get_xml());

	// tests reset method
	CPPUNIT_ASSERT(!br_org_delete_cmd->get_id().empty());
	CPPUNIT_ASSERT(!br_org_delete_cmd->get_organization().empty());
	br_org_delete_cmd->reset();
	CPPUNIT_ASSERT_EQUAL((string)"", br_org_delete_cmd->get_organization());

	// parent attribute
	CPPUNIT_ASSERT(br_org_delete_cmd->get_id().empty());
	CPPUNIT_ASSERT(br_org_delete_cmd->get_clTRID() == "");
}

void BrOrgDeleteTest::command_test()
{
	BrOrgDelete br_org_delete;
	BrOrgDeleteCmd *br_org_delete_cmd = br_org_delete.get_command();

	br_org_delete_cmd->set_id("btw214");
	br_org_delete_cmd->set_organization("005.506.560/0001-36");

	bool exception_caught = false;
	try {
		string xml_template = 
			FileUtil::read_file("../docs/templates/br_org_delete.xml");

		br_org_delete.get_command()->set_clTRID("ABC-12345");
		br_org_delete.set_xml_template(xml_template);

		DomParser parser;
		parser.enable_validation("../docs/schemas");
		parser.parse_command(br_org_delete.get_xml());
	} catch (const IoException &e) {
		exception_caught = true;
		printf("\nIO Exception: code [%d] message [%s]",
		       e.get_code(), e.get_msg().c_str());
	} catch (const XmlException &e) {
		exception_caught = true;
		printf("\nXml Exception: code [%d] message [%s] low level message [%s]\n",
		       e.get_code(), e.get_msg().c_str(), e.get_low_level_msg().c_str());
	}

	CPPUNIT_ASSERT(!exception_caught);
}

#endif //USE_BR_ORG
