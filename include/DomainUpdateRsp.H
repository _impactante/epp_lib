/* 
 * Copyright (C) 2006-2015 Registro.br. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistribution of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY REGISTRO.BR ``AS IS AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIE OF FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL REGISTRO.BR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 */
/* $Id$ */
/** @file  DomainUpdateRsp.H
 *  @brief EPP DomainUpdateRsp Class
 */

#ifndef __DOMAIN_UPDATE_RSP_H__
#define __DOMAIN_UPDATE_RSP_H__

#include "libepp_nicbr.H"

#include "CommonData.H"
#include "Response.H"

LIBEPP_NICBR_NS_BEGIN

/// EPP DomainUpdateRsp Class
class DomainUpdateRsp : public Response
{
public:
	/// Default constructor
	DomainUpdateRsp(bool reset = true) : Response(false)
	{
		if (reset) {
			this->reset();
		}
	}

	//******************** RFC 3915 BEGIN ********************
	// Sets RGP status
	/**
	   @param status RGP Status
	 */
	void set_rgpStatus(const RGPStatus::Value status)
	{
		_rgp_status = status;
	}

	/// Returns RGP status
	/**
	   @return RGP status
	 */
	RGPStatus::Value get_rgpStatus() const
	{
		return _rgp_status;
	}
	//******************** RFC 3915 END ********************

	/// Reset object attributes
	void reset()
	{
		Response::reset();
		_rgp_status = RGPStatus::NONE;
	}

protected:
	/// RGP status
	RGPStatus::Value _rgp_status;
};

LIBEPP_NICBR_NS_END

#endif // __DOMAIN_UPDATE_RSP_H__
