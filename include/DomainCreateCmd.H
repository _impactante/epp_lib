/* 
 * Copyright (C) 2006-2015 Registro.br. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistribution of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY REGISTRO.BR ``AS IS AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIE OF FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL REGISTRO.BR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 */
/* $Id: DomainCreateCmd.H 1181 2013-07-25 19:45:41Z rafael $ */
/** @file  DomainCreateCmd.H
 *  @brief EPP DomainCreateCmd Class
 */

#ifndef __DOMAIN_CREATE_CMD_H__
#define __DOMAIN_CREATE_CMD_H__

#include <string>
#include <vector>
#include <map>
#include <list>

#include "libepp_nicbr.H"

#include "Command.H"
#include "CommonData.H" // struct NameServer
#include "DSInfo.H"
#include "RegistrationPeriod.H"
#include "LaunchCreateCmd.H"

using std::string;
using std::set;
using std::map;
using std::list;
using std::less;

LIBEPP_NICBR_NS_BEGIN

/// EPP DomainCreateCmd Class
class DomainCreateCmd : public Command
{
public:
	/// Default constructor
	DomainCreateCmd(bool reset = true) : Command(false)
	{
		if (reset) {
			this->reset();
		}
	}

	/// Sets domain name
	/**
	   @param name   fully qualified domain name
	*/
	void set_name(const string& name) { _name = name; }

	/// Returns domain name
	/**
	   @return fully qualified domain name
	*/
	string get_name() const { return _name; }

	/// Sets domain registration period
	/**
	   @param time   amount of time
	   @param unit   measure unit
	*/
	void set_period(const int time, const string& unit)
	{
		_period.time = time;
		_period.unit = unit;
	}

	/// Returns domain registration period
	/**
	   @return domain registration period
	*/
	RegistrationPeriod get_period() const { return _period; }

	/// Inserts a nameserver to the list of nameservers
	/**
	   @param nameserver   a name server
	*/
	void insert_nameserver(const struct NameServer &nameserver)
	{
		_nameservers.push_back(nameserver);
	}

	/// Returns a list of nameservers
	/**
	   @return list of nameservers associated with domain object
	*/
	vector<struct NameServer> get_nameservers() const { return _nameservers; }

	/// Sets registrant
	/**
	   @param registrant   registrant identification
	*/
	void set_registrant(const string& registrant) { _registrant = registrant; }

	/// Returns registrant
	/**
	   @return registrant identification
	*/
	string get_registrant() const { return _registrant; }

	/// Inserts a contact in the map of other contacts
	/**
	   @param type             contact type
	   @param identification   contact identification
	*/
	void insert_contact(const string& type, const string& identification)
	{
		_contacts[type] = identification;
	}

	/// Returns map of other contacts
	/**
	   @return map of other contacts
	*/
	map< string, string, less<string> > get_contacts() const { return _contacts; }

	/// Sets authorization information
	/**
	   @param authInfo   domain authorization information
	*/
	void set_authInfo(const AuthInfo &authInfo)
	{
		_authInfo = authInfo;
	}
  
	/// Returns authorization information
	/**
	   @return authorization information
	*/
	AuthInfo get_authInfo() const { return _authInfo; }

	//******************** RFC 4310/5910 BEGIN ********************
	/// Sets secDNS extension version
	/**
	   @param secDNS extension version
	*/
	void set_secDnsVersion(string secDnsVersion)
	{
		_secDnsVersion = secDnsVersion;
	}

	/// Returns secDNS extension version
	/**
	   @return secDNS extension version
	*/
	string get_secDnsVersion() const
	{
		return _secDnsVersion;
	}

	///  Adds DS information
	/**
	   @param DS information
	*/
	void add_dsInfo(const DSInfo &ds_info)
	{
		_ds_list.push_back(ds_info);
	}
  
	/// Returns DS information list
	/**
	   @return DS information list
	*/
	list<DSInfo> get_dsInfo() const
	{
		return _ds_list;
	}

	///  Adds DNSKEY information
	/**
	   @param DNSKEY information
	*/
	void add_keyData(const KeyData &dnskey_data)
	{
		_dnskey_list.push_back(dnskey_data);
	}
  
	/// Returns DNSKEY information list
	/**
	   @return DNSKEY information list
	*/
	list<KeyData> get_keyDataList() const
	{
		return _dnskey_list;
	}

	/// Sets the maximum signature life
	/**
	   @param max_sig_life the maximum signature life 
	*/
	void set_max_sig_life(const unsigned int max_sig_life)
	{ 
		_max_sig_life = max_sig_life;
	}

	/// Returns the maximum signature life
	/**
	   @return maximum signature life
	*/
	unsigned int get_max_sig_life() const 
	{
		return _max_sig_life;
	}

	/// Sets the launch
	/**
	 * @param launch launch
	 */
	void set_launch(const LaunchCreateCmd &launch) { _launch = launch; }

	/// Returns the launch
	/**
	 * @return launch
	 */
	LaunchCreateCmd get_launch() const { return _launch; }

	//******************** RFC 4310/5910 END ********************

	/// Check if there is any extension
	bool has_extension() const {
		return has_secdns_extension() || has_launch_extension();
	}
  
	/// Check if there is secDNS extension
	bool has_secdns_extension() const {
		bool hasSecDns10 = _secDnsVersion == "1.0" && !_ds_list.empty();

		bool hasSecDns11 = _secDnsVersion == "1.1" &&
			!(_ds_list.empty() && _dnskey_list.empty());

		return (hasSecDns10 || hasSecDns11);
	}

	/// Check if there is launch extension
	bool has_launch_extension() const {
		return _launch.get_phase().get_phase() != LaunchPhase::NONE;
	}

	/// Reset object attributes
	void reset()
	{
		Command::reset();
		_name = "";
		_period.time = 0;
		_period.unit = "";
		_nameservers.clear();
		_registrant = "";
		_contacts.clear();
		_authInfo.reset();
		_secDnsVersion = "1.1";
		_max_sig_life = 0;
		_ds_list.clear(); 
		_dnskey_list.clear();
		_launch.reset();
	}
  
protected:
	/// fully qualified domain name
	string _name;

	/// initial registration period of the domain object
	RegistrationPeriod _period;

	/// name servers associated with domain object
	vector<struct NameServer> _nameservers;

	/// registrant
	string _registrant;

	/// other contact objects
	map< string, string, less<string> > _contacts;

	/// Authorization information
	AuthInfo _authInfo;

	/// RFC 4310/5910 secDNS extension version
	string _secDnsVersion;

	/// RRSIG max signature life
	unsigned int _max_sig_life;

	/// DS info
	list<DSInfo> _ds_list;

	/// KeyData
	list<KeyData> _dnskey_list;

	/// Launch
	LaunchCreateCmd _launch;
};

LIBEPP_NICBR_NS_END
#endif //__DOMAIN_CREATE_CMD_H__
