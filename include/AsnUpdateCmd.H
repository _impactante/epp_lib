/* 
 * Copyright (C) 2006-2015 Registro.br. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistribution of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY REGISTRO.BR ``AS IS AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIE OF FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL REGISTRO.BR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 */
/* $Id: AsnUpdateCmd.H 1245 2014-11-17 16:50:01Z rafael $ */
/** @file AsnUpdateCmd.H
 *  @brief EPP AsnUpdateCmd Class
 */

#ifndef __ASNUPDATECMD_H__
#define __ASNUPDATECMD_H__

#include <map>
#include <string>
#include <vector>

#include "libepp_nicbr.H"

#include "Command.H"

using std::map;
using std::string;
using std::vector;

LIBEPP_NICBR_NS_BEGIN

/// EPP AsnUpdate Class
class AsnUpdateCmd : public Command
{
public:
	/// Default constructor
	AsnUpdateCmd(bool reset = true) : Command(false)
	{
		if (reset) {
			this->reset();
		}
	}

	/// Sets asn
	/**
	   @param asn   autonomous system number
	*/
	void set_asn(const int asn)
	{
		_asn = asn;
	}

	/// Returns asn
	/**
	   @return autonomous system number
	*/
	int get_asn() const
	{
		return _asn;
	}

	/// Sets organization
	/**
	   @param organization   organization to be associated with the asn
	*/
	void set_organization(const string &organization)
	{
		_organization = organization;
	}

	/// Returns organization associated with the asn
	/**
	   @return organization owner of the asn
	*/
	string get_organization() const
	{
		return _organization;
	}

	void set_creation_date(const string &creation_date)
	{
		_creation_date = creation_date;
	}

	string get_creation_date() const
	{
		return _creation_date;
	}

	/// Inserts a contact to be added
	/**
	   @param type             contact type
	   @param identification   contact identification
	*/
	void insert_contact_add(const string &type, const string &identification)
	{
		_contacts_add[type] = identification;
	}

	/// Returns map of other contacts to be added
	/**
	   @return map of other contacts
	*/
	map< string, string, less<string> > get_contacts_add() const
	{
		return _contacts_add;
	}

	/// Inserts a contact to be removed
	/**
	   @param type             contact type
	   @param identification   contact identification
	*/
	void insert_contact_rem(const string &type, const string &identification)
	{
		_contacts_rem[type] = identification;
	}

	/// Returns map of other contacts to be removed
	/**
	   @return map of other contacts
	*/
	map< string, string, less<string> > get_contacts_rem() const
	{
		return _contacts_rem;
	}

	/// Inserts an AS input policy to be added
	/**
	   @param policy AS policy
	 */
	void insert_as_in_add(const string &policy)
	{
		_as_in_add.push_back(policy);
	}

	/// Returns list of all AS input policies to be added
	/**
	   @return list of policies
	 */
	vector<string> get_as_in_add() const
	{
		return _as_in_add;
	}

	/// Inserts an AS input policy to be removed
	/**
	   @param policy AS policy
	 */
	void insert_as_in_rem(const string &policy)
	{
		_as_in_rem.push_back(policy);
	}

	/// Returns list of all AS input policies to be removed
	/**
	   @return list of policies
	 */
	vector<string> get_as_in_rem() const
	{
		return _as_in_rem;
	}

	/// Inserts an AS output policy to be added
	/**
	   @param policy AS policy
	 */
	void insert_as_out_add(const string &policy)
	{
		_as_out_add.push_back(policy);
	}

	/// Returns list of all AS output policies to be added
	/**
	   @return list of policies
	 */
	vector<string> get_as_out_add() const
	{
		return _as_out_add;
	}

	/// Inserts an AS output policy to be removed
	/**
	   @param policy AS policy
	 */
	void insert_as_out_rem(const string &policy)
	{
		_as_out_rem.push_back(policy);
	}

	/// Returns list of all AS output policies to be removed
	/**
	   @return list of policies
	 */
	vector<string> get_as_out_rem() const
	{
		return _as_out_rem;
	}

	void reset()
	{
		Command::reset();
		_asn = 0;
		_organization = "";
		_contacts_add.clear();
		_contacts_rem.clear();
		_creation_date.clear();
		_as_in_add.clear();
		_as_in_rem.clear();
		_as_out_add.clear();
		_as_out_rem.clear();
	}

protected:
	/// autonomous system number
	int _asn;

	/// organization associated with the asn
	string _organization;

	/// object creation date
	string _creation_date;

	/// contacts to be added
	map< string, string, less<string> > _contacts_add;

	/// contacts to be removed
	map< string, string, less<string> > _contacts_rem;

	/// as-in policy to be added
	vector<string> _as_in_add;

	/// as-in policy to be removed
	vector<string> _as_in_rem;

	// as-out policy to be added
	vector<string> _as_out_add;

	// as-out policy to be removed
	vector<string> _as_out_rem;
};

LIBEPP_NICBR_NS_END
#endif // __ASNUPDATECMD_H__
