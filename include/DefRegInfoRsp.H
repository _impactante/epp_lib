/* 
 * Copyright (C) 2006-2015 Registro.br. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistribution of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY REGISTRO.BR ``AS IS AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIE OF FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL REGISTRO.BR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 */
/* $Id$ */
/** @file DefRegInfoRsp.H
 *  @brief EPP DefRegInfoRsp Class
 */

#ifndef __DEF_REG_INFO_RSP_H__
#define __DEF_REG_INFO_RSP_H__

#include <list>
#include <string>

#include "libepp_nicbr.H"

#include "Response.H"
#include "CommonData.H"

using std::list;
using std::string;

LIBEPP_NICBR_NS_BEGIN

/// EPP DefRegInfoRsp Class
class DefRegInfoRsp : public Response
{
public:
	/// Default constructor
	DefRegInfoRsp(bool reset = true) : Response(false)
	{
		if (reset) {
			this->reset();
		}
	}

	/// Sets repository object identification
	/**
	   @param roid   repository object identification
	*/
	void set_roid(const string& roid) { _roid = roid; }

	/// Returns repository object identification
	/**
	   @return repository object identification
	*/
	string get_roid() const { return _roid; }

	/// Sets name of the Defensive Registration object
	/**
	   @param name name of the Defensive Registration object
	*/
	void set_name(const DefRegName &name) { _name = name; }

	/// Returns name of the Defensive Registration object
	/**
	   @return name of the Defensive Registration object
	*/
	DefRegName get_name() const { return _name; }

	/// Sets registrant
	/**
	   @param registrant   registrant identification
	*/
	void set_registrant(const string& registrant) { _registrant = registrant; }

	/// Returns registrant
	/**
	   @return registrant identification
	*/
	string get_registrant() const { return _registrant; }

	/// Sets the Trademark Identifier (ID) associated with the Defensive
	/// Registration Object
	/**
	 * @param id trademark identifier
	 */
	void set_trademark_id(const string &id) { _trademark_id = id; }

	/// Returns the trademark Identifier (ID) associated with the
	/// Defensive Registration Object
	/**
	 * @return trademark identifier
	 */
	string get_trademark_id() const { return _trademark_id; }

	/// Sets the country which issued the Trademark associated with the
	/// Defensive Registration Object
	/**
	 * @param country country
	 */
	void set_trademark_country(const string &country) { _trademark_country = country; }

	/// Returns country which issued the Trademark associated with the
	/// Defensive Registration Object
	/**
	 * @return country
	 */
	string get_trademark_country() const { return _trademark_country; }

	/// Sets date when the Trademark was issued
	/**
	 * @param date date when the Trademark was issued
	 */
	void set_trademark_date(const string &date) { _trademark_date = date; }

	/// Returns the date when the Trademark was issued
	/**
	 * @return date when the Trademark was issued
	 */
	string get_trademark_date() const { return _trademark_date; }

	/// Sets the identifier for the administrator associated with the
	/// Defensive Registration object
	/**
	 * @param contact administrator contact id
	 */
	void set_admin_contact(const string &contact) { _admin_contact = contact; }

	/// Returns the identifier for the administrator associated with the
	/// Defensive Registration object
	/**
	 * @return administrator contact id
	 */
	string get_admin_contact() const { return _admin_contact; }

	/// Inserts a new status
	/**
	 * @param status status to be inserted
	 */
	void insert_status(const string& status) { _status_set.insert(status); }

	/// Returns set of status
	/**
	 * @return set of status
	 */
	set<string> get_status_set() const { return _status_set; }

	/// Sets the sponsoring client
	/**
	 * @param clID sponsoring client
	 */
	void set_clID(const string& clID)
	{
		_clID = clID;
	}

	/// Returns sponsoring client
	/**
	   @return sponsoring client
	*/
	string get_clID() const { return _clID; }

	/// Sets the identifier of the client that created the object
	/**
	   @param crID   id of the client that created the object
	*/
	void set_crID(const string& crID) { _crID = crID; }

	/// Returns the name of the Defensive Registration object
	/**
	   @return name of the Defensive Registration object
	*/
	string get_crID() const { return _crID; }

	/// Sets creation date
	/**
	   @param crDate   object creation date
	*/
	void set_crDate(const string& crDate) { _crDate = crDate; }

	/// Returns creation date
	/**
	   @return creation date
	*/
	string get_crDate() const { return _crDate; }

	/// Sets the identifier of the client that last updated the object
	/**
	   @param upID   id of the client that created the object
	*/
	void set_upID(const string& upID) { _upID = upID; }

	/// Returns the identifier of the client that last updated the object
	/**
	   @return id of client that last updated the object
	*/
	string get_upID() const { return _upID; }

	/// Sets last modification date
	/**
	   @param upDate   last modification date
	*/
	void set_upDate(const string& upDate) { _upDate = upDate; }

	/// Returns last modification date
	/**
	   @return last modification date
	*/
	string get_upDate() const { return _upDate; }

	/// Sets expiration date
	/**
	   @param exDate expiration date
	*/
	void set_exDate(const string& exDate) { _exDate = exDate; }

	/// Returns expiration date
	/**
	   @return expiration date
	*/
	string get_exDate() const { return _exDate; }

	/// Sets last successfull transfer date
	/**
	   @param trDate   last successfull transfer date
	*/
	void set_trDate(const string& trDate) { _trDate = trDate; }

	/// Returns last successfull transfer date
	/**
	   @return last successfull transfer date
	*/
	string get_trDate() const { return _trDate; }

	/// Sets authorization information
	/**
	   @param authInfo authorization information
	*/
	void set_authInfo(const AuthInfo &authInfo)
	{
		_authInfo = authInfo;
	}

	/// Returns authorization information
	/**
	   @return authorization information
	*/
	AuthInfo get_authInfo() const { return _authInfo; }

	/// Resets object attributes
	void reset() 
	{
		Response::reset();
		_name.reset();
		_roid = "";
		_status_set.clear();
		_registrant = "";
		_trademark_id = "";
		_trademark_country = "";
		_trademark_date = "";
		_admin_contact = "";
		_clID = "";
		_crID = "";
		_crDate = "";
		_upID = "";
		_upDate = "";
		_exDate = "";
		_upDate = "";
		_trDate = "";
		_authInfo.reset();
	}

protected:
	/// Contains the Repository Object Identifier assigned to the
	/// Defensive Registration object when the object was created
	string _roid;

	/// Name of the Defensive Registration object
	DefRegName _name;

	/// Identifier for the human or organizational social information
	/// (contact) object to be associated with the Defensive
	/// Registration object as the object registrant
	string _registrant;

	/// Trademark Identifier (ID) associated with the Defensive
	/// Registration Object
	string _trademark_id;

	/// Country which issued the Trademark associated with the Defensive
	/// Registration Object
	string _trademark_country;

	/// Date when the Trademark was issued
	string _trademark_date;

	/// Identifier for the administrator associated with the Defensive
	/// Registration object
	string _admin_contact;

	/// Contain the current status descriptors associated with the
	/// Defensive Registration
	set<string> _status_set;

	/// sponsoring client id
	string _clID;

	/// client that created object
	string _crID;

	/// creation date
	string _crDate;

	/// client that last updated object
	string _upID;

	/// last modification date
	string _upDate;

	/// expiration date
	string _exDate;

	/// last successfull transfer date
	string _trDate;

	/// authorization information
	AuthInfo _authInfo;
};

LIBEPP_NICBR_NS_END

#endif // __DEF_REG_INFO_RSP_H__
