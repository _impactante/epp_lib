/* 
 * Copyright (C) 2006-2015 Registro.br. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistribution of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY REGISTRO.BR ``AS IS AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIE OF FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL REGISTRO.BR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 */
/* $Id$ */
/** @file  Launch.H
 *  @brief EPP Launch Phase
 */

#ifndef __LAUNCH_H__
#define __LAUNCH_H__

#include <list>
#include <string>

#include "SMD.H"

using std::list;
using std::string;

LIBEPP_NICBR_NS_BEGIN

/// EPP CodeMark Class
class CodeMark
{
public:
	/// Default constructor
	CodeMark()
	{
		reset();
	}

	/// Sets the code used to validate the <mark:mark> information
	/**
	 * @param code code used to validate the <mark:mark> information
	 */
	void set_code(const string &code) { _code = code; }

	/// Returns the code used to validate the <mark:mark> information
	/**
	 * @return used to validate the <mark:mark> information
	 */
	string get_code() const { return _code; }

	/// Sets the mark information
	/**
	 * @param mark mark information
	 */
	void set_mark(const SMDMark &mark) { _mark = mark; }

	/// Returns the mark information
	/**
	 * @return mark information
	 */
	SMDMark get_mark() const { return _mark; }

	/// Reset object attributes
	void reset()
	{
		_code.clear();
		_mark.reset();
	}	

private:
	/// Code used to validate the <mark:mark> information.  The mark
	/// code is be a mark-specific secret that the server can verify
	/// against a third party
	string _code;

	/// Mark information
	SMDMark _mark;
};

/// EPP EncodedSignedMark Class
class EncodedSignedMark
{
public:
	/// Default constructor
	EncodedSignedMark()
	{
		reset();
	}

	/// Sets the encoding of the data
	/**
	 * @param encoding encoding of the data
	 */
	void set_encoding(const string &encoding) { _encoding = encoding; }

	/// Returns the encoding of the data
	/**
	 * @return encoding of the data
	 */
	string get_encoding() const { return _encoding; }

	/// Sets the signed mark encoded data
	/**
	 * @params signed mark encoded data
	 */
	void set_data(const string &data) { _data = data; }

	/// Returns the signed mark encoded data
	/**
	 * @return signed mark encoded data
	 */
	string get_data() const { return _data; }

	/// Reset object attributes
	void reset()
	{
		_encoding.clear();
		_data.clear();
	}	

private:
	/// Encoding of the data
	string _encoding;
	/// Signed mark encoded data
	string _data;
};


/// EPP Launch Phase Class
class LaunchPhase
{
public:
	/// Possible phase values
	enum Value {
		NONE,
		SUNRISE,
		LANDRUSH,
		CLAIMS,
		OPEN,
		CUSTOM
	};

	/// Convert phase to text format
	/**
	 * @param value phase value
	 * @return text representation of the phase
	 */
	static string toStr(const Value value)
	{
		switch (value) {
		case NONE:
			break;
		case SUNRISE:
			return "sunrise";
		case LANDRUSH:
			return "landrush";
		case CLAIMS:
			return "claims";
		case OPEN:
			return "open";
		case CUSTOM:
			return "custom";
		}

		return "";
	}

	/// Convert text phase to value
	/**
	 * @param value text phase
	 * @return value representation of the phase
	 */
	static Value fromStr(const string &value)
	{
		if (value == "sunrise") {
			return SUNRISE;

		} else if (value == "landrush") {
			return LANDRUSH;

		} else if (value == "claims") {
			return CLAIMS;
				
		} else if (value == "open") {
			return OPEN;

		} else if (value == "custom") {
			return CUSTOM;
		}

		return NONE;
	}

	/// Default constructor
	LaunchPhase()
	{
		reset();
	}

	/// Constructor used when the phase is not CUSTOM
	/**
	 * @param phase launch phase
	 */
	LaunchPhase(const Value phase) :
		_phase(phase),
		_name("")
	{
	}

	/// Constructor used when the phase is CUSTOM
	/**
	 * @param name name of the custom phase
	 */
	LaunchPhase(const string &name) :
		_phase(CUSTOM),
		_name(name)
	{
	}

	/// Sets the launch phase
	/**
	 * @param phase launch phase
	 */
	void set_phase(const Value phase) { _phase = phase; }

	/// Returns the launch phase
	/**
	 * @return launch phase
	 */
	Value get_phase() const { return _phase; }

	/// Sets the launch phase name when the phase is CUSTOM
	/**
	 * @param name launch phase name when the phase is CUSTOM
	 */
	void set_name(const string &name) { _name = name; }

	/// Returns the launch phase name when the phase is CUSTOM
	/**
	 * @return launch phase name when the phase is CUSTOM
	 */
	string get_name() const { return _name; }

	/// Reset object attributes
	void reset()
	{
		_phase = NONE;
		_name.clear();
	}

private:
	/// Launch phase
	Value _phase;

	/// Launch phase name when the phase is CUSTOM
	string _name;
};

LIBEPP_NICBR_NS_END

#endif // __LAUNCH_H__
