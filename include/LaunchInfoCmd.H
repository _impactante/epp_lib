/* 
 * Copyright (C) 2006-2015 Registro.br. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistribution of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY REGISTRO.BR ``AS IS AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIE OF FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL REGISTRO.BR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 */
/* $Id$ */
/** @file  LaunchInfoCmd.H
 *  @brief EPP LaunchInfoCmd Class
 */

#ifndef __LAUNCH_INFO_CMD_H__
#define __LAUNCH_INFO_CMD_H__

#include <string>

#include "Launch.H"

using std::string;

LIBEPP_NICBR_NS_BEGIN

/// EPP LaunchInfoCmd Class
class LaunchInfoCmd
{
public:
	// Default constructor
	LaunchInfoCmd()
	{
		reset();
	}

	/// Sets whether or not to include the mark in the response
	/**
	 * @param includeMark whether or not to include the mark in the response
	 */
	void set_includeMark(const bool includeMark) { _includeMark = includeMark; }

	/// Returns whether or not to include the mark in the response
	/**
	 * @return whether or not to include the mark in the response
	 */
	bool is_markIncluded() const { return _includeMark; }

	/// Sets the phase of the launch
	/**
	 * @param phase phase of the launch
	 */
	void set_phase(const LaunchPhase &phase) { _phase = phase; }

	/// Returns the phase of the launch
	/**
	 * @return phase of the launch
	 */
	LaunchPhase get_phase() const { return _phase; }

	/// Sets the application identifier of the Launch Application
	/**
	 * @param applicationId application identifier of the Launch Application
	 */
	void set_applicationId(const string &applicationId) { _applicationId = applicationId; }

	/// Returns the application identifier of the Launch Application
	/**
	 * @return application identifier of the Launch Application
	 */
	string get_applicationId() const { return _applicationId; }

	/// Reset object attributes
	void reset()
	{
		_includeMark = false;
		_phase.reset();
		_applicationId.clear();
	}

private:
	/// Indicate whether or not to include the mark in the response
	bool _includeMark;

	/// Phase of the launch
	LaunchPhase _phase;

	/// Application identifier of the Launch Application
	string _applicationId;
};

LIBEPP_NICBR_NS_END

#endif // __LAUNCH_INFO_CMD_H__
