/* 
 * Copyright (C) 2006-2015 Registro.br. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistribution of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY REGISTRO.BR ``AS IS AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIE OF FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL REGISTRO.BR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 */
/* $Id$ */

#ifndef __DOMAIN_TRANSFER_RSP_H__
#define __DOMAIN_TRANSFER_RSP_H__

#include <string>

#include "libepp_nicbr.H"

#include "Response.H"

using std::string;

LIBEPP_NICBR_NS_BEGIN

/// EPP DomainTransferRsp Class
class DomainTransferRsp : public Response
{
public:
  
	/// Default constructor
	DomainTransferRsp(bool reset = true) : Response(false)
	{
		if (reset) {
			this->reset();
		}
	}

	/// Sets fully qualified name of the domain object
	/**
	   @param name domain name
	*/
	void set_name(const string name)
	{
		_name = name;
	}

	/// Returns fully qualified name of the domain object
	/**
	   @return domain name
	*/
	string get_name() const
	{
		return _name;
	}

	/// Sets state of the most recent transfer request
	/**
	   @param trStatus transfer status
	*/
	void set_trStatus(const string &trStatus)
	{
		_trStatus = trStatus;
	}

	/// Returns state of the most recent transfer request
	/**
	   @return transfer status
	*/
	string get_trStatus() const
	{
		return _trStatus;
	}

	/// Sets identifier of the client that requested the object transfer
	/**
	   @param reID request ID
	*/
	void set_reID(const string &reID)
	{
		_reID = reID;
	}

	/// Returns identifier of the client that requested the object
	/// transfer
	/**
	   @return request ID
	*/
	string get_reID() const
	{
		return _reID;
	}

	/// Sets date and time that the transfer was requested
	/**
	   @param reDate transfer reqeust date
	*/
	void set_reDate(const string &reDate)
	{
		_reDate = reDate;
	}

	/// Returns date and time that the transfer was requested
	/**
	   @return transfer reqeust date
	*/
	string get_reDate() const
	{
		return _reDate;
	}
  
	/// Sets identifier of the client that SHOULD act upon a PENDING transfer
	/// request
	/**
	   @param acID identifier of the client
	*/
	void set_acID(const string &acID)
	{
		_acID = acID;
	}

	/// Returns identifier of the client that SHOULD act upon a PENDING
	/// transfer request
	/**
	   @return identifier of the client
	*/
	string get_acID() const
	{
		return _acID;
	}

	/// Sets date and time of a required or completed response
	/**
	   @param acDate date
	*/
	void set_acDate(const string &acDate)
	{
		_acDate = acDate;
	}

	/// Returns date and time of a required or completed response
	/**
	   @return date
	*/
	string get_acDate() const
	{
		return _acDate;
	}

	/// Sets the end of the domain object's validity period if the
	/// <transfer> command caused or causes a change in the validity
	/// period
	/**
	   @param exDate expiration date
	*/
	void set_exDate(const string &exDate)
	{
		_exDate = exDate;
	}

	/// Returns end of the domain object's validity period if the
	/// <transfer> command caused or causes a change in the validity
	/// period
	/**
	   @return expiration date
	*/
	string get_exDate() const
	{
		return _exDate;
	}

	/// reset attributes
	void reset() 
	{ 
		Response::reset();
		_name = "";
		_trStatus = "";
		_reID = "";
		_reDate = "";
		_acID = "";
		_acDate = "";
		_exDate = "";
	}
  
protected:
	/// Server-unique identifier for the queried domain
	string _name;

	/// State of the most recent transfer request
	string _trStatus;

	/// Identifier of the client that requested the object transfer
	string _reID;

	/// Date and time that the transfer was requested
	string _reDate;

	/// Identifier of the client that SHOULD act upon a PENDING transfer
	/// request
	string _acID;

	/// Date and time of a required or completed response
	string _acDate;

	/// End of the domain object's validity period
	string _exDate;
};

LIBEPP_NICBR_NS_END
#endif // __DOMAIN_TRANSFER_RSP_H__
