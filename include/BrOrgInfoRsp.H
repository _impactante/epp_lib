/* 
 * Copyright (C) 2006-2015 Registro.br. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistribution of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY REGISTRO.BR ``AS IS AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIE OF FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL REGISTRO.BR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 */
/* $Id: BrOrgInfoRsp.H 1260 2014-12-02 17:38:55Z rafael $ */
/** @file BrOrgInfoRsp.H
 *  @brief EPP BrOrgInfoRsp Class
 */

#ifndef __BR_ORG_INFO_RSP_H__
#define __BR_ORG_INFO_RSP_H__

#include <string>
#include <map>
#include <set>
#include <memory>

#include "libepp_nicbr.H"

#include "ContactInfoRsp.H"
#include "CommonData.H"

using std::string;
using std::map;
using std::set;
using std::auto_ptr;

LIBEPP_NICBR_NS_BEGIN

/// EPP BrOrgInfoRsp Class
class BrOrgInfoRsp : public ContactInfoRsp
{
public:
	/// Default constructor
	BrOrgInfoRsp(bool reset = true) : ContactInfoRsp(false)
	{
		if (reset) {
			this->reset();
		}
	}

	/// Copies data from the parent
	void copy_parent_data(ContactInfoRsp &parent)
	{
		_roid = parent.get_roid();
		_status_set = parent.get_status_set();
		_clID = parent.get_clID();
		_crID = parent.get_crID();
		_crDate = parent.get_crDate();
		_upID = parent.get_upID();
		_upDate = parent.get_upDate();
		_trDate = parent.get_trDate();
		_common = parent.get_common_data();
		_authInfo = parent.get_authInfo();
	}

	/// Sets the Organization
	/**
	   @param organization Organization Identifier
	*/
	void set_organization(const string &organization)
	{
		_organization = organization;
	}

	/// Returns the Organization
	/**
	   @return Organization Identifier
	*/
	string get_organization()
	{
		return _organization;
	}


	/// Sets Responsible for the organization
	/**
	   @param responsible   Person Responsible
	*/
	void set_responsible(const string &responsible)
	{
		_responsible = responsible;
	}

	/// Returns Responsible for the organization
	/**
	   @return Person Responsible
	*/
	string get_responsible()
	{
		return _responsible;
	};

	/// Inserts a Contact into the list
	/**
	   @param id Contact Id
	   @param type Contact Type
	*/
	void insert_contact(const string &type, const string &id)
	{
		_contact_list[type] = id;
	}

	/// Returns the Contact List
	/**
	   @return Contact List
	*/
	map< string, string, less<string> > get_contact_list()
	{
		return _contact_list;
	}

	/// Sets the Legal Representative
	/**
	   @param proxy Local Legal Representative
	*/
	void set_proxy(const string &proxy)
	{
		_proxy = proxy;
	}

	/// Returns the Legal Representative
	/**
	   @return Local Legal Representative
	*/
	string get_proxy()
	{
		return _proxy;
	}

	/// Inserts a domain name into the list
	/**
	   @param fqdn fully qualified domain name
	*/
	void insert_domainName(const string &fqdn)
	{
		_domain_list.insert(fqdn);
	}

	/// Returns the domain name list
	/**
	   @return domain name list
	*/
	set<string> get_domainName_list()
	{
		return _domain_list;
	}

	/// Sets  date and  time  identifying the  end  of the  organization
	/// object's registration period
	/**
	   @param exDate expiration date
	*/
	void set_exDate(const string &exDate)
	{
		_exDate = exDate;
	}

	/// Returns date  and time identifying  the end of  the organization
	/// object's registration period
	/**
	   @return expiration date
	*/
	string get_exDate() const
	{
		return _exDate;
	}

	/// Inserts   Autonomous   System   object  associated   with   this
	/// organization
	void insert_asn(const int &asn)
	{
		_asn_list.insert(asn);
	}

	/// Returns   Autonomous   System   object  associated   with   this
	/// organization
	set<int> get_asn_list() const
	{
		return _asn_list;
	}

	/// Inserts  IP addresses  of network  objects associated  with this
	/// organization
	void insert_ipRange(const IpRange &ipRange)
	{
		_ipRange_list.insert(ipRange);
	}

	/// Returns  IP addresses  of network  objects associated  with this
	/// organization
	set<IpRange> get_ipRange_list() const
	{
		return _ipRange_list;
	}

	/// Sets suspended flag for organizations with payment problems
	/*
	  @param suspended flag
	*/
	void set_suspended(const bool suspended)
	{
		if (suspended) {
			_suspended = SuspendedStatus::SUSPENDED;

		} else {
			_suspended = SuspendedStatus::NORMAL;
		}
	}

	/// Returns the suspended flag for organizations with payment
	/// problems
	/*
	  @return suspended flag
	 */
	SuspendedStatus::Value get_suspended() const
	{
		return _suspended;
	}

	/////////////////////////////////////////////
	// LACNIC Org extension methods

	/// Sets the organization type
	/**
	   @param type organization type
	 */
	void set_type(const string &type)
	{
		_type = type;
	}

	/// Returns the organization type
	/**
	   @return organization type
	 */
	string get_type() const
	{
		return _type;
	}

	/// Sets the organization epp status
	/**
	   @param epp_status organization epp status
	 */
	void set_epp_status(const string &epp_status)
	{
		_epp_status = epp_status;
	}

	/// Returns the organization epp status
	/**
	   @return organization epp status
	 */
	string get_epp_status() const
	{
		return _epp_status;
	}

	/// Inserts an EPP IP or range that will be allowed in the EPP
	/// server
	/*
	  @param ip IP or range
	 */
	void insert_epp_ip(const string &ip)
	{
		_epp_ips.push_back(ip);
	}

	/// Returns list of IPs or ranges that will be allowed in the EPP
	/// server
	/*
	  @return list of EPP IPs or ranges
	 */
	vector<string> get_epp_ips() const
	{
		return _epp_ips;
	}

	/// Inserts a renewal type
	/*
	  @param type renewal type
	 */
	void insert_renewal_type(const string &type)
	{
		_renewal_types.push_back(type);
	}

	/// Sets the organization category and characteristics
	/*
	  @param types renewal types
	 */
	void set_renewal_types(const vector<string> &types)
	{
		_renewal_types = types;
	}

	/// Returns list of renewal types of an organization
	/*
	  @return list of renewal types of an organization
	 */
	vector<string> get_renewal_types() const
	{
		return _renewal_types;
	}

	/// Sets the organization billing date
	/*
	  @param date renewal date
	*/
	void set_renewal_date(const string &date)
	{
		_renewal_date = date;
	}

	/// Returns the organization billing date
	/*
	  @return renewal date
	*/
	string get_renewal_date() const
	{
		return _renewal_date;
	}

	/// Sets the organization resources class
	/*
	  @param resources_class for now can be "all-resources" or "non-legacy-only"
	 */
	void set_resources_class(const string &resources_class)
	{
		_resources_class = resources_class;
	}

	/// Returns the organization resources class
	/*
	  @return organization resources class
	 */
	string get_resources_class() const
	{
		return _resources_class;
	}

	/// Sets the legacy organization password
	/*
	  @param password legacy organization password
	 */
	void set_password(const string &password)
	{
		_password = password;
	}

	/// Returns the legacy organization password
	/*
	  @return legacy organization password
	 */
	string get_password() const
	{
		return _password;
	}

	/// reset attributes
	void reset()
	{
		ContactInfoRsp::reset();
		_organization = "";
		_responsible = "";
		_proxy = "";
		_contact_list.clear();
		_domain_list.clear();
		_exDate = "";
		_asn_list.clear();
		_ipRange_list.clear();
		_suspended = SuspendedStatus::UNDEFINED;
		_type = "";
		_epp_status = "";
		_epp_ips.clear();
		_renewal_types.clear();
		_renewal_date = "";
		_resources_class = "";
		_password = "";
	}

protected:
	/// Organization Identifier
	string _organization;

	/// Person responsible for the organization
	string _responsible;

	/// Human Contacts
	map<string, string, less<string> > _contact_list;

	/// Local Legal Representative
	string _proxy;

	/// Domain names
	set<string> _domain_list;

	/// Expiration date
	string _exDate;

	/// ASNs
	set<int> _asn_list;

	// IP Ranges
	set<IpRange> _ipRange_list;

	/// Suspended flag
	SuspendedStatus::Value _suspended;

	/////////////////////////////////////////////
	// LACNIC Org extension methods

	/// Define the organization type
	string _type;

	/// Determine if the organization is blocked for EPP access
	string _epp_status;

	/// IP addresses or ranges that are allowed to connect to the EPP
	/// interface
	vector<string> _epp_ips;

	/// List of category and characteristics of the organization
	vector<string> _renewal_types;

	/// Billing date for organizations that have resources
	string _renewal_date;

	/// There are some options when categorizing an organization. We can
	/// use all resouces to categorize or only the non legacy
	/// resources. The possible values are "all-resources" and
	/// "non-legacy-only"
	string _resources_class;

	/// Authenticate updates of legacy organizations
	string _password;
};

LIBEPP_NICBR_NS_END
#endif //__BR_ORG_INFO_RSP_H__
