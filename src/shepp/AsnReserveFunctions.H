/* 
 * Copyright (C) 2006-2015 Registro.br. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistribution of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY REGISTRO.BR ``AS IS AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIE OF FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL REGISTRO.BR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 */
/* $Id: AsnReserveFunctions.H 1245 2014-11-17 16:50:01Z rafael $ */
/** @file AsnReserveFunctions.H
 *  @brief EPP ASN Reserve related functions
 */
#ifndef __ASN_RESERVE_FUNCTIONS_H__
#define __ASN_RESERVE_FUNCTIONS_H__
#include "SheppCommandFunctions.H"

#include "AsnReserveCreate.H"
#include "AsnReserveDelete.H"

/// print asn reserve command usage info
/**
   @param error_msg   error message to be printed
   @param specific    optional, choses specific sub-command
   @return 0 if ok, -1 otherwise
*/
int cmd_asn_reserve_help(string error_msg, string specific = "")
{
	if (error_msg != "") {
		printf("error: %s\n", error_msg.c_str());
	}

	printf("asnreserve command syntax help:\n");
	printf("\n");

	if (specific == "" || specific == "create") {
		printf("  create <-o organization> <-start value> [-end value] "
		       "[-comment value]\n");
		printf("\n");
	}
	if (specific == "" || specific == "delete") {
		printf("  delete <id>\n");
		printf("\n");
	}

	if (error_msg != "") {
		return -1;
	}

	return 0;
}

/// asn create command function
/**
   @param args   vector with command-line arguments
   @return 0 if ok, -1 otherwise
*/
int cmd_asn_reserve_create(vector<string> &args)
{
	string cmd_name = "create";

	AsnReserveCreate act;
	AsnReserveCreateCmd *cmd = act.get_command();

	if (args.empty()) {
		return cmd_asn_reserve_help("no arguments specified", cmd_name);
	}

	bool hasStart = false;
	bool hasEnd = false;
	bool hasOrganization = false;
	bool hasComment = false;

	while (!args.empty()) {
		if (args[0] == "-start") {
			//set start of the range
			if (hasStart) {
				return cmd_asn_reserve_help("only one Start ASN allowed per command",
				                            cmd_name);
			} else {
				hasStart = true;
			}
			args.erase(args.begin());
			if (args.empty()) {
				return cmd_asn_reserve_help("start parameter missing", cmd_name);
			}

			int start = atoi(args[0].c_str());
			if (start <= 0) {
				return cmd_asn_reserve_help("invalid Start ASN: '" + args[0] + "'",
				                            cmd_name);
			}

			cmd->set_start_asn(start);
			args.erase(args.begin());
		} else if (args[0] == "-end") {
			//set end of the range
			if (hasEnd) {
				return cmd_asn_reserve_help("only one End ASN allowed per command",
				                    cmd_name);
			} else {
				hasEnd = true;
			}
			args.erase(args.begin());
			if (args.empty()) {
				return cmd_asn_reserve_help("end parameter missing", cmd_name);
			}

			int end = atoi(args[0].c_str());
			if (end <= 0) {
				return cmd_asn_reserve_help("invalid End ASN: '" + args[0] + "'",
				                            cmd_name);
			}

			cmd->set_end_asn(end);
			args.erase(args.begin());
		} else if (args[0] == "-o") {
			//set organization
			if (hasOrganization) {
				return cmd_asn_reserve_help("only one organization allowed per command",
				                            cmd_name);
			} else {
				hasOrganization = true;
			}
			args.erase(args.begin());
			if (args.empty()) {
				return cmd_asn_reserve_help("organization parameter missing", cmd_name);
			}
			cmd->set_organization(args[0]);
			args.erase(args.begin());
		} else if (args[0] == "-comment") {
			args.erase(args.begin());
			if (args.empty()) {
				return cmd_asn_reserve_help("comment parameter missing", cmd_name);
			}

			string comment = args[0];
			args.erase(args.begin());

			if (SheppStrUtil::quote_gathering(args, comment) != 0) {
				return cmd_asn_reserve_help("error setting comment", cmd_name);
			}

			cmd->set_comment(comment);

		} else {
			return cmd_asn_reserve_help("invalid syntax near \"" + args[0] + "\"",
			                            cmd_name);
		}
	}

	if (!hasOrganization) {
		return cmd_asn_reserve_help("no organization specified", cmd_name);
	}

	if (!hasStart) {
		return cmd_asn_reserve_help("no start ASN specified", cmd_name);	
	}

	if (hasStart && !hasEnd) {
		cmd->set_end_asn(cmd->get_start_asn());
	}

	if (_debug) {
		printf("ASN Reserve to be created: [%d-%d]\n", cmd->get_start_asn(),
		       cmd->get_end_asn());
		printf("organization: [%s]\n", cmd->get_organization().c_str());

		if (cmd->get_comment() != "") {
			printf("comment: [%s]\n", cmd->get_comment().c_str());			
		}
	} // _debug

	if (process_action(act) != 0) {
		return -1;
	}

	return 0;
}

/// asn reserve delete function
/**
   @param args   vector with command-line arguments
   @return 0
*/
int cmd_asn_reserve_delete(vector<string> &args)
{
	string cmd_name = "delete";

	AsnReserveDelete act;
	AsnReserveDeleteCmd *cmd = act.get_command();

	if (args.size() != 1) {
		return cmd_asn_reserve_help("exactly one ASN Reserve id must be specified", cmd_name);
	}

	int id = atoi(args[0].c_str());
	if (id <= 0) {
		return cmd_asn_reserve_help("invalid ASN: '" + args[0] + "'", cmd_name);
	}

	cmd->set_id(id);

	if (_debug) {
		printf("ASN Reserve to be deleted: [%d]\n", cmd->get_id());
	} // _debug

	if (process_action(act) != 0) {
		return -1;
	}

	return 0;
}

/// main asn reserve command
/**
   @param arg   command-line input arguments
   @return 0 if ok, -1 otherwise
*/
int cmd_asn_reserve(vector<string> &args)
{
	// asn reserve command processing
	if (!args.empty() && !(args[0] == "help")) {
		if (args[0] == "create") {
			args.erase(args.begin());
			return cmd_asn_reserve_create(args);
		} else if (args[0] == "delete") {
			args.erase(args.begin());
			return cmd_asn_reserve_delete(args);
		} else {
			return cmd_asn_reserve_help("invalid command: asnreserve " + args[0]);
		}
	}
  
	return cmd_asn_reserve_help("");
}

#endif //__ASN_RESERVE_FUNCTIONS_H__
