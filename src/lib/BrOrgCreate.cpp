/* 
 * Copyright (C) 2006-2015 Registro.br. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistribution of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY REGISTRO.BR ``AS IS AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIE OF FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL REGISTRO.BR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 */
/* $Id: BrOrgCreate.cpp 1254 2014-11-25 16:39:51Z rafael $ */
#if USE_BR_ORG
#include "config.h"

#include "BrOrgCreate.H"
#include "StrUtil.H"


LIBEPP_NICBR_NS_BEGIN

string fill_lacnic_org_output(BrOrgCreateCmd *cmd)
{
	string output("");

	if (cmd->get_type().empty() &&
	    cmd->get_epp_password().empty() &&
	    cmd->get_epp_ips().empty() &&
	    cmd->get_renewal_types().empty() &&
	    cmd->get_resources_class().empty()) {
		return output;
	}

	StrUtil su;
	output += "<lacnicorg:create "
		"xmlns:lacnicorg='urn:ietf:params:xml:ns:lacnicorg-1.0' "
		"xsi:schemaLocation='urn:ietf:params:xml:ns:lacnicorg-1.0 "
		"lacnicorg-1.0.xsd'>";

	if (!cmd->get_type().empty()) {
		output += "<lacnicorg:type>" + su.esc_xml_markup(cmd->get_type()) +
			"</lacnicorg:type>";
	}

	if (!cmd->get_epp_password().empty()) {
		output += "<lacnicorg:eppPassword>" + 
			su.esc_xml_markup(cmd->get_epp_password()) +
			"</lacnicorg:eppPassword>";
	}

	vector<string> eppIPs = cmd->get_epp_ips();
	for (int i = 0; i < eppIPs.size(); i++) {
		output += "<lacnicorg:eppIP>" + su.esc_xml_markup(eppIPs[i]) + 
			"</lacnicorg:eppIP>";
	}

	vector<string> renewalTypes = cmd->get_renewal_types();
	for (int i = 0; i < renewalTypes.size(); i++) {
		output += "<lacnicorg:renewalType>" + su.esc_xml_markup(renewalTypes[i]) + 
			"</lacnicorg:renewalType>";
	}

	if (!cmd->get_resources_class().empty()) {
		output += "<lacnicorg:resourcesClass>" +
			su.esc_xml_markup(cmd->get_resources_class()) +
			"</lacnicorg:resourcesClass>";
	}

	output += "</lacnicorg:create>";

	return output;
}

void BrOrgCreate::set_xml_template(const string &xml_template)
{
	StrUtil su;
	ContactCreate::set_xml_template(xml_template);

	map < string, string, less<string> > to_parse;
  
	to_parse["organization"] =
		su.esc_xml_markup(get_command()->get_organization());

	map< string, string, less<string> >::const_iterator it;
	map< string, string, less<string> > contact_list =
		get_command()->get_contact_list();
	for (it = contact_list.begin(); it != contact_list.end(); it++) {
		to_parse["brorg_contact_list"] += "<brorg:contact type='" +
			su.esc_xml_markup((*it).first) + "'>" +
			su.esc_xml_markup((*it).second) + "</brorg:contact>";
	}

	to_parse["responsible"] = "";
	bool responsible_f = get_command()->get_responsible_f();
	if (responsible_f == true) {
		string responsible = get_command()->get_responsible();
		to_parse["responsible"] = 
			"<brorg:responsible>" + su.esc_xml_markup(responsible) +
			"</brorg:responsible>";
	}

	to_parse["lacnicorg_ext"] = fill_lacnic_org_output(get_command());

	_xml = StrUtil::parse(_xml, to_parse, "$(", ")$");
}

LIBEPP_NICBR_NS_END
#endif //USE_BR_ORG
