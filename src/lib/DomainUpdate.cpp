/* 
 * Copyright (C) 2006-2015 Registro.br. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistribution of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY REGISTRO.BR ``AS IS AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIE OF FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL REGISTRO.BR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 */
/* $Id: DomainUpdate.cpp 1187 2013-07-30 19:09:21Z rafael $ */

#include "config.h"

#include "DomainUpdate.H"
#include "StrUtil.H"

LIBEPP_NICBR_NS_BEGIN

string fill_launch_output(DomainUpdateCmd *cmd)
{
	string output("");

	LaunchUpdateCmd launch = cmd->get_launch();
	if (launch.get_phase().get_phase() == LaunchPhase::NONE) {
		return output;
	}

	StrUtil su;

	output += "<launch:update xmlns:launch=\"urn:ietf:params:xml:ns:launch-1.0\">"
		"<launch:phase";

	LaunchPhase phase = launch.get_phase();
	if (!phase.get_name().empty()) {
		output += " name=\"" + su.esc_xml_markup(phase.get_name()) + "\"";
	}

	output += ">" + LaunchPhase::toStr(phase.get_phase()) + "</launch:phase>"
		"<launch:applicationID>" + su.esc_xml_markup(launch.get_applicationId()) +
		"</launch:applicationID>"
		"</launch:update>";
	return output;
}

void DomainUpdate::set_xml_template(const string &xml_template)
{
	StrUtil su;
	Action::set_xml_template_common(xml_template);
	map < string, string, less<string> > to_parse;

	//name
	to_parse["name"] = su.esc_xml_markup(get_command()->get_name());

	//aux containers and iterators
	vector<struct NameServer> ns_cnt;
	vector<struct NameServer>::const_iterator it_ns;
	set<NSIPAddr>::const_iterator it_ip;
	map< string, string, less<string> > contact_cnt;
	map< string, string, less<string> >::const_iterator it_contact;
	set<struct DomainUpdateCmd::Status> status_cnt;
	set<struct DomainUpdateCmd::Status>::const_iterator it_status;

	//stuff to add
	ns_cnt = get_command()->get_nameserver_add();
	contact_cnt = get_command()->get_contact_add();
	status_cnt = get_command()->get_status_add();

	to_parse["add"] = "";
	if (ns_cnt.size() > 0 ||
	    contact_cnt.size() > 0 ||
	    status_cnt.size() > 0) {

		to_parse["add"] = "<domain:add>";

		//nameservers to add
		if (ns_cnt.size() > 0) {
			to_parse["add"] += "<domain:ns>";
			for (it_ns = ns_cnt.begin(); it_ns != ns_cnt.end();
			     it_ns++) {
				to_parse["add"] += "<domain:hostAttr>";
				to_parse["add"] += "<domain:hostName>" +
					su.esc_xml_markup(it_ns->name) + "</domain:hostName>";
				for (it_ip = it_ns->ips.begin(); it_ip != it_ns->ips.end();
				     it_ip++) {
					to_parse["add"] += "<domain:hostAddr";
					if ((*it_ip).version != "") {
						to_parse["add"] += " ip='" + su.esc_xml_markup((*it_ip).version) +
							"'";
					}
					to_parse["add"] += ">" + su.esc_xml_markup((*it_ip).addr) +
						"</domain:hostAddr>";
				}
				to_parse["add"] += "</domain:hostAttr>";
			}
			to_parse["add"] += "</domain:ns>";
		}

		//contacts to add
		if (contact_cnt.size() > 0) {
			for (it_contact = contact_cnt.begin();
			     it_contact != contact_cnt.end(); it_contact++) {
				to_parse["add"] += "<domain:contact type='" +
					su.esc_xml_markup((*it_contact).first) + "'>" +
					su.esc_xml_markup((*it_contact).second) +
					"</domain:contact>";
			}
		}

		//status to add
		if (status_cnt.size() > 0) {
			for (it_status = status_cnt.begin(); it_status != status_cnt.end();
			     it_status++) {
				to_parse["add"] += "<domain:status s='" +
					su.esc_xml_markup((*it_status).s) + "' lang='" +
					su.esc_xml_markup((*it_status).lang) + "'>" +
					su.esc_xml_markup((*it_status).msg) + "</domain:status>";
			}
		}

		to_parse["add"] += "</domain:add>";
	}

	//stuff to remove
	ns_cnt = get_command()->get_nameserver_rem();
	contact_cnt = get_command()->get_contact_rem();
	status_cnt = get_command()->get_status_rem();

	to_parse["rem"] = "";
	if (ns_cnt.size() > 0 ||
	    contact_cnt.size() > 0 ||
	    status_cnt.size() > 0) {

		to_parse["rem"] = "<domain:rem>";

		//nameservers to remove
		if (ns_cnt.size() > 0) {
			to_parse["rem"] += "<domain:ns>";
			for (it_ns = ns_cnt.begin(); it_ns != ns_cnt.end();
			     it_ns++) {
				to_parse["rem"] += "<domain:hostAttr>";
				to_parse["rem"] += "<domain:hostName>" +
					su.esc_xml_markup(it_ns->name) + "</domain:hostName>";
				for (it_ip = it_ns->ips.begin(); it_ip != it_ns->ips.end();
				     it_ip++) {
					to_parse["rem"] += "<domain:hostAddr";
					if ((*it_ip).version != "") {
						to_parse["rem"] += " ip='" + su.esc_xml_markup((*it_ip).version) +
							"'";
					}
					to_parse["rem"] += ">" + su.esc_xml_markup((*it_ip).addr) +
						"</domain:hostAddr>";
				}
				to_parse["rem"] += "</domain:hostAttr>";
			}
			to_parse["rem"] += "</domain:ns>";
		}

		//contacts to remove
		if (contact_cnt.size() > 0) {
			for (it_contact = contact_cnt.begin();
			     it_contact != contact_cnt.end(); it_contact++) {
				to_parse["rem"] += "<domain:contact type='" +
					su.esc_xml_markup((*it_contact).first) + "'>" +
					su.esc_xml_markup((*it_contact).second) +
					"</domain:contact>";
			}
		}

		//status to remove
		if (status_cnt.size() > 0) {
			for (it_status = status_cnt.begin(); it_status != status_cnt.end();
			     it_status++) {
				to_parse["rem"] += "<domain:status s='" +
					su.esc_xml_markup((*it_status).s) + "'/>";
			}
		}

		to_parse["rem"] += "</domain:rem>";
	}

	//stuff to change
	to_parse["chg"] = "";
	bool registrant_f = get_command()->get_registrant_f();
	AuthInfo authInfo = get_command()->get_authInfo();

	if (registrant_f || authInfo.get_pw_f() || authInfo.get_roid_f()) {
		//registrant
		string registrant_str = su.esc_xml_markup(get_command()->get_registrant());

		if (registrant_f) {
			registrant_str = "<domain:registrant>" + registrant_str
				+ "</domain:registrant>";
		}

		//authInfo
		string auth_info_str = "";

		if (authInfo.get_pw_f()) {
			auth_info_str = "<domain:authInfo>";
			if (authInfo.get_pw() == "\"\"") {
				auth_info_str += "<domain:null/>";
			} else {
				auth_info_str += "<domain:pw>" + su.esc_xml_markup(authInfo.get_pw()) +
					"</domain:pw>";
			}
			auth_info_str += "</domain:authInfo>";
		}

		to_parse["chg"] = "<domain:chg>" + registrant_str + auth_info_str +
			"</domain:chg>";
	}

	to_parse["ext_begin"] = "";
	to_parse["ext_end"] = "";
	to_parse["ds_ext"] = "";
	to_parse["rgp"] = "";
	to_parse["launch_ext"] = "";

	if (get_command()->has_extension()) {
		to_parse["ext_begin"] = "<extension>";
		to_parse["ext_end"] = "</extension>";

		/////////////////////////////////////////////////////////////////////
		// secDNS Extension Mapping - RFC 4310/5910
		// DNSSEC Extension
		if (get_command()->has_secdns_extension()) {
			list<DSInfo> ds_add_list = get_command()->get_ds_add();
			list<DSInfo> ds_rem_list = get_command()->get_ds_rem_1_1();
			list<unsigned int> ds_rem_list_1_0 = get_command()->get_ds_rem();
			list<DSInfo> ds_chg_list = get_command()->get_ds_chg();
			list<KeyData> dnskey_add_list = get_command()->get_dnskey_add();
			list<KeyData> dnskey_rem_list = get_command()->get_dnskey_rem();

			if (get_command()->get_secDnsVersion() == "1.0") {
				to_parse["ds_ext"] += "<secDNS:update ";
				if (get_command()->isUrgent()) {
					to_parse["ds_ext"] += "urgent=\"1\" ";
				}
				to_parse["ds_ext"] +=
					"xmlns:secDNS=\"urn:ietf:params:xml:ns:secDNS-1.0\" "
					"xsi:schemaLocation=\"urn:ietf:params:xml:ns:secDNS-1.0 "
					"secDNS-1.0.xsd\">";

				list<DSInfo>::const_iterator it;

				if (!ds_add_list.empty()) {
					// DS Add List
					to_parse["ds_ext"] += "<secDNS:add>";
					for (it = ds_add_list.begin(); it != ds_add_list.end(); it++) {
						to_parse["ds_ext"] += it->get_xml_format();
					}
					to_parse["ds_ext"] += "</secDNS:add>";
				} else if (!ds_rem_list_1_0.empty()) {
					// DS Remove list
					to_parse["ds_ext"] += "<secDNS:rem>";
					list<unsigned int>::const_iterator myKeyTag;
					for (myKeyTag = ds_rem_list_1_0.begin();
					     myKeyTag != ds_rem_list_1_0.end(); myKeyTag++) {
						to_parse["ds_ext"] += "<secDNS:keyTag>" +
							su.to_string("%u", *myKeyTag) + "</secDNS:keyTag>";
					}
					to_parse["ds_ext"] += "</secDNS:rem>";
				} else if (!ds_chg_list.empty()) {
					// DS Change List
					to_parse["ds_ext"] += "<secDNS:chg>";
					for (it = ds_chg_list.begin(); it != ds_chg_list.end(); it++) {
						to_parse["ds_ext"] += it->get_xml_format();
					}
					to_parse["ds_ext"] += "</secDNS:chg>";
				}

				to_parse["ds_ext"] += "</secDNS:update>";
			} else if (get_command()->get_secDnsVersion() == "1.1") {
				to_parse["ds_ext"] += "<secDNS:update ";
				if (get_command()->isUrgent()) {
					to_parse["ds_ext"] += "urgent=\"true\" ";
				}
				to_parse["ds_ext"] +=
					"xmlns:secDNS=\"urn:ietf:params:xml:ns:secDNS-1.1\" "
					"xsi:schemaLocation=\"urn:ietf:params:xml:ns:secDNS-1.1 "
					"secDNS-1.1.xsd\">";

				// alternative "remove all" option
				if (get_command()->getRemoveAll() == true) {
					to_parse["ds_ext"] +=
						"<secDNS:rem><secDNS:all>true</secDNS:all></secDNS:rem>";
				}

				// prioritizes KeyData over DSInfo
				if (!dnskey_add_list.empty() ||
				    (!dnskey_rem_list.empty() &&
				     get_command()->getRemoveAll() == false)) {
					list<KeyData>::iterator it;

					if (!dnskey_rem_list.empty() && get_command()->getRemoveAll() == false) {
						to_parse["ds_ext"] += "<secDNS:rem>";
						for (it = dnskey_rem_list.begin(); it != dnskey_rem_list.end();
						     it++) {
							to_parse["ds_ext"] += it->get_xml_format();
						}
						to_parse["ds_ext"] += "</secDNS:rem>";
					}

					if (!dnskey_add_list.empty()) {
						to_parse["ds_ext"] += "<secDNS:add>";
						for (it = dnskey_add_list.begin(); it != dnskey_add_list.end();
						     it++) {
							to_parse["ds_ext"] += it->get_xml_format();
						}
						to_parse["ds_ext"] += "</secDNS:add>";
					}
				} else if (!ds_add_list.empty() ||
				           (!ds_rem_list.empty() &&
				            get_command()->getRemoveAll() == false)) {
					list<DSInfo>::iterator it;

					if (!ds_rem_list.empty() && get_command()->getRemoveAll() == false) {
						to_parse["ds_ext"] += "<secDNS:rem>";
						for (it = ds_rem_list.begin(); it != ds_rem_list.end(); it++) {
							to_parse["ds_ext"] +=
								it->get_xml_format(get_command()->get_secDnsVersion());
						}
						to_parse["ds_ext"] += "</secDNS:rem>";
					}

					if (!ds_add_list.empty()) {
						to_parse["ds_ext"] += "<secDNS:add>";
						for (it = ds_add_list.begin(); it != ds_add_list.end(); it++) {
							to_parse["ds_ext"] +=
								it->get_xml_format(get_command()->get_secDnsVersion());
						}
						to_parse["ds_ext"] += "</secDNS:add>";
					}
				}

				if (get_command()->get_max_sig_life() > 0) {
					to_parse["ds_ext"] += "<secDNS:chg>";
					to_parse["ds_ext"] += "<secDNS:maxSigLife>" +
						StrUtil::to_string("%u", get_command()->get_max_sig_life()) +
						"</secDNS:maxSigLife>";
					to_parse["ds_ext"] += "</secDNS:chg>";
				}

				to_parse["ds_ext"] += "</secDNS:update>";
			}
		}

		RGPRestore::Operation::Value rgp_op = 
			get_command()->get_rgp_restore().get_operation();
		if (rgp_op != RGPRestore::Operation::NONE) {
			to_parse["rgp"] = "<rgp:update xmlns:rgp=\"urn:ietf:params:xml:ns:rgp-1.0\" "
				"xsi:schemaLocation=\"urn:ietf:params:xml:ns:rgp-1.0 "
				"rgp-1.0.xsd\">";

			string rgp_op_str = RGPRestore::Operation::toString(rgp_op);
			if (rgp_op == RGPRestore::Operation::REQUEST) {
				to_parse["rgp"] += "<rgp:restore op=\"" + rgp_op_str + "\"/>";

			} else if (rgp_op == RGPRestore::Operation::REPORT) {
				RGPReport report = get_command()->get_rgp_restore().get_report();

				to_parse["rgp"] += "<rgp:restore op=\"" + rgp_op_str + "\">"
					"<rgp:report>"
					"<rgp:preData>" + su.esc_xml_markup(report.get_pre_data()) + "</rgp:preData>"
					"<rgp:postData>" + su.esc_xml_markup(report.get_post_data()) +
					"</rgp:postData>"
					"<rgp:delTime>" + su.esc_xml_markup(report.get_del_time()) + "</rgp:delTime>"
					"<rgp:resTime>" + su.esc_xml_markup(report.get_res_time()) + "</rgp:resTime>"
					"<rgp:resReason>" + su.esc_xml_markup(report.get_res_reason()) +
					"</rgp:resReason>";

				if (!report.get_statement1_lang().empty()) {
					to_parse["rgp"] += "<rgp:statement lang=\"" +
						su.esc_xml_markup(report.get_statement1_lang()) + "\">";
				} else {
					to_parse["rgp"] += "<rgp:statement>";
				}

				to_parse["rgp"] += su.esc_xml_markup(report.get_statement1()) +
					"</rgp:statement>";

				if (!report.get_statement2_lang().empty()) {
					to_parse["rgp"] += "<rgp:statement lang=\"" +
						su.esc_xml_markup(report.get_statement2_lang()) + "\">";
				} else {
					to_parse["rgp"] += "<rgp:statement>";
				}

				to_parse["rgp"] += su.esc_xml_markup(report.get_statement2()) +
					"</rgp:statement>";

				if (!report.get_other().empty()) {
					to_parse["rgp"] += "<rgp:other>" + su.esc_xml_markup(report.get_other()) +
						"</rgp:other>";
				}

				to_parse["rgp"] += "</rgp:report>"
					"</rgp:restore>";
			}

			to_parse["rgp"] += "</rgp:update>";
		}

		if (get_command()->has_launch_extension()) {
			to_parse["launch_ext"] = fill_launch_output(get_command());
		}
	}

	_xml = StrUtil::parse(_xml, to_parse, "$(", ")$");
}

LIBEPP_NICBR_NS_END
