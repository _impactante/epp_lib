/* 
 * Copyright (C) 2006-2015 Registro.br. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistribution of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY REGISTRO.BR ``AS IS AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIE OF FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL REGISTRO.BR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 */
/* $Id: DomainCreate.cpp 1190 2013-08-16 22:18:04Z cesar $ */

#include "config.h"

#include "DomainCreate.H"
#include "StrUtil.H"

LIBEPP_NICBR_NS_BEGIN

string fill_dnssec_output(DomainCreateCmd *cmd)
{
	string output("");

	if (!cmd->has_secdns_extension()) {
		return output;
	}

	///////////////////////////////////////////
	// secDNS Extension Mapping - RFC 4310/5910

	list<DSInfo> ds_info_list = cmd->get_dsInfo();
	list<KeyData> dnskey_data_list = cmd->get_keyDataList();

	if (cmd->get_secDnsVersion() == "1.0") {
		output +=  "<secDNS:create "
			"xmlns:secDNS=\"urn:ietf:params:xml:ns:secDNS-1.0\" "
			"xsi:schemaLocation=\"urn:ietf:params:xml:ns:secDNS-1.0 "
			"secDNS-1.0.xsd\">";

		list<DSInfo>::iterator it;
		for (it = ds_info_list.begin(); it != ds_info_list.end(); it++) {
			output += it->get_xml_format(cmd->get_secDnsVersion());
		}

		output += "</secDNS:create>";

	} else if (cmd->get_secDnsVersion() == "1.1") {
		output +=  "<secDNS:create "
			"xmlns:secDNS=\"urn:ietf:params:xml:ns:secDNS-1.1\" "
			"xsi:schemaLocation=\"urn:ietf:params:xml:ns:secDNS-1.1 "
			"secDNS-1.1.xsd\">";

		if (cmd->get_max_sig_life() > 0) {
			output += "<secDNS:maxSigLife>" +
				StrUtil::to_string("%u", cmd->get_max_sig_life()) +
				"</secDNS:maxSigLife>";
		}

		// prioritizes KeyData over DSInfo
		if (!dnskey_data_list.empty()) {
			list<KeyData>::iterator it;
			for (it = dnskey_data_list.begin(); it != dnskey_data_list.end(); it++) {
				output += it->get_xml_format();
			}
		} else {
			list<DSInfo>::iterator it;
			for (it = ds_info_list.begin(); it != ds_info_list.end(); it++) {
				output += it->get_xml_format(cmd->get_secDnsVersion());
			}
		}

		output += "</secDNS:create>";
	}

	return output;
}

string fill_holder_output(const SMDHolder::Type::Value type, const SMDHolder &holder)
{
	StrUtil su;
	string output("");

	output += "<mark:holder entitlement=\"" + 
		su.esc_xml_markup(SMDHolder::Type::toStr(type)) + "\">";

	if (!holder.get_name().empty()) {
		output += "<mark:name>" + su.esc_xml_markup(holder.get_name()) + "</mark:name>";
	}

	if (!holder.get_org().empty()) {
		output += "<mark:org>" + su.esc_xml_markup(holder.get_org()) + "</mark:org>";
	}

	output += "<mark:addr>";

	PostalInfo postalInfo = holder.get_postalInfo();
	
	if (!postalInfo.get_str1().empty()) {
		output += "<mark:street>" + 
			su.esc_xml_markup(postalInfo.get_str1()) + "</mark:street>";
	}

	if (!postalInfo.get_str2().empty()) {
		output += "<mark:street>" + 
			su.esc_xml_markup(postalInfo.get_str2()) + "</mark:street>";
	}

	if (!postalInfo.get_str3().empty()) {
		output += "<mark:street>" + 
			su.esc_xml_markup(postalInfo.get_str3()) + "</mark:street>";
	}

	output += "<mark:city>" + su.esc_xml_markup(postalInfo.get_city()) + "</mark:city>";

	if (!postalInfo.get_sp().empty()) {
		output += "<mark:sp>" + su.esc_xml_markup(postalInfo.get_sp()) + "</mark:sp>";
	}

	if (!postalInfo.get_pc().empty()) {
		output += "<mark:pc>" + su.esc_xml_markup(postalInfo.get_pc()) + "</mark:pc>";
	}

	output += "<mark:cc>" + su.esc_xml_markup(postalInfo.get_cc()) + "</mark:cc>"
		"</mark:addr>";

	if (!holder.get_voice().empty()) {
		output += "<mark:voice>" + su.esc_xml_markup(holder.get_voice()) + "</mark:voice>";
	}

	if (!holder.get_fax().empty()) {
		output += "<mark:fax>" + su.esc_xml_markup(holder.get_fax()) + "</mark:fax>";
	}

	if (!holder.get_email().empty()) {
		output += "<mark:email>" + su.esc_xml_markup(holder.get_email()) + "</mark:email>";
	}

	output += "</mark:holder>";

	return output;
}

string fill_contact_output(const SMDContact::Type::Value type, const SMDContact &contact)
{
	StrUtil su;
	string output("");

	output += "<mark:contact type=\"" + 
		su.esc_xml_markup(SMDContact::Type::toStr(type)) + "\">"
		"<mark:name>" + su.esc_xml_markup(contact.get_name()) + "</mark:name>";

	if (!contact.get_org().empty()) {
		output += "<mark:org>" + su.esc_xml_markup(contact.get_org()) + "</mark:org>";
	}

	output += "<mark:addr>";

	PostalInfo postalInfo = contact.get_postalInfo();
	
	if (!postalInfo.get_str1().empty()) {
		output += "<mark:street>" + 
			su.esc_xml_markup(postalInfo.get_str1()) + "</mark:street>";
	}

	if (!postalInfo.get_str2().empty()) {
		output += "<mark:street>" + 
			su.esc_xml_markup(postalInfo.get_str2()) + "</mark:street>";
	}

	if (!postalInfo.get_str3().empty()) {
		output += "<mark:street>" + 
			su.esc_xml_markup(postalInfo.get_str3()) + "</mark:street>";
	}

	output += "<mark:city>" + su.esc_xml_markup(postalInfo.get_city()) + "</mark:city>";

	if (!postalInfo.get_sp().empty()) {
		output += "<mark:sp>" + su.esc_xml_markup(postalInfo.get_sp()) + "</mark:sp>";
	}

	if (!postalInfo.get_pc().empty()) {
		output += "<mark:pc>" + su.esc_xml_markup(postalInfo.get_pc()) + "</mark:pc>";
	}

	output += "<mark:cc>" + su.esc_xml_markup(postalInfo.get_cc()) + "</mark:cc>"
		"</mark:addr>"
		"<mark:voice>" + su.esc_xml_markup(contact.get_voice()) + "</mark:voice>";

	if (!contact.get_fax().empty()) {
		output += "<mark:fax>" + su.esc_xml_markup(contact.get_fax()) + "</mark:fax>";
	}

	output += "<mark:email>" + su.esc_xml_markup(contact.get_email()) + "</mark:email>"
		"</mark:contact>";
	return output;
}

string fill_trademark_output(const SMDTrademark &trademark)
{
	StrUtil su;
	string output("");

	output += "<mark:trademark>"
		"<mark:id>" + su.esc_xml_markup(trademark.get_id()) + "</mark:id>"
		"<mark:markName>" + su.esc_xml_markup(trademark.get_markName()) + "</mark:markName>";

	list<pair<SMDHolder::Type::Value, SMDHolder> > holders = trademark.get_holders();
	list<pair<SMDHolder::Type::Value, SMDHolder> >::const_iterator holderIt;

	for (holderIt = holders.begin(); holderIt != holders.end(); holderIt++) {
		output += fill_holder_output(holderIt->first, holderIt->second);
	}

	map<SMDContact::Type::Value, SMDContact> contacts = trademark.get_contacts();
	map<SMDContact::Type::Value, SMDContact>::const_iterator contactIt;

	for (contactIt = contacts.begin(); contactIt != contacts.end(); contactIt++) {
		output += fill_contact_output(contactIt->first, contactIt->second);
	}

	output += "<mark:jurisdiction>" + 
		su.esc_xml_markup(trademark.get_jurisdiction()) + "</mark:jurisdiction>";

	list<string> classes = trademark.get_classes();
	list<string>::const_iterator classIt;

	for (classIt = classes.begin(); classIt != classes.end(); classIt++) {
		output += "<mark:class>" + su.esc_xml_markup(*classIt) + "</mark:class>";
	}

	list<string> labels = trademark.get_labels();
	list<string>::const_iterator labelIt;

	for (labelIt = labels.begin(); labelIt != labels.end(); labelIt++) {
		output += "<mark:label>" + su.esc_xml_markup(*labelIt) + "</mark:label>";
	}

	output += "<mark:goodsAndServices>" + 
		su.esc_xml_markup(trademark.get_goodsAndServices()) + "</mark:goodsAndServices>";

	if (!trademark.get_apId().empty()) {
		output += "<mark:apId>" + su.esc_xml_markup(trademark.get_apId()) + "</mark:apId>";
	}

	if (!trademark.get_apDate().empty()) {
		output += "<mark:apDate>" + 
			su.esc_xml_markup(trademark.get_apDate()) + "</mark:apDate>";
	}

	output += "<mark:regNum>" + 
		su.esc_xml_markup(trademark.get_regNum()) + "</mark:regNum>"
		"<mark:regDate>" + su.esc_xml_markup(trademark.get_regDate()) + "</mark:regDate>";

	if (!trademark.get_exDate().empty()) {
		output += "<mark:exDate>" + 
			su.esc_xml_markup(trademark.get_exDate()) + "</mark:exDate>";
	}

	output += "</mark:trademark>";
	return output;
}

string fill_protection_output(const SMDProtection &protection)
{
	StrUtil su;
	string output("");

	output += "<mark:protection>"
		"<mark:cc>" + su.esc_xml_markup(protection.get_cc()) + "</mark:cc>";

	if (!protection.get_region().empty()) {
		output += "<mark:region>" + 
			su.esc_xml_markup(protection.get_region()) + "</mark:region>";
	}

	list<string> rulings = protection.get_rulings();
	list<string>::const_iterator rulingIt;

	for (rulingIt = rulings.begin(); rulingIt != rulings.end(); rulingIt++) {
		output += "<mark:ruling>" + su.esc_xml_markup(*rulingIt) + "</mark:ruling>";
	}

	output += "</mark:protection>";
	return output;
}

string fill_treaty_or_statute_output(const SMDTreatyOrStatute &treatyOrStatute)
{
	StrUtil su;
	string output("");

	output += "<mark:treatyOrStatute>"
		"<mark:id>" + su.esc_xml_markup(treatyOrStatute.get_id()) + "</mark:id>"
		"<mark:markName>" + 
		su.esc_xml_markup(treatyOrStatute.get_markName()) + "</mark:markName>";

	list<pair<SMDHolder::Type::Value, SMDHolder> > holders = treatyOrStatute.get_holders();
	list<pair<SMDHolder::Type::Value, SMDHolder> >::const_iterator holderIt;

	for (holderIt = holders.begin(); holderIt != holders.end(); holderIt++) {
		output += fill_holder_output(holderIt->first, holderIt->second);
	}

	map<SMDContact::Type::Value, SMDContact> contacts = treatyOrStatute.get_contacts();
	map<SMDContact::Type::Value, SMDContact>::const_iterator contactIt;

	for (contactIt = contacts.begin(); contactIt != contacts.end(); contactIt++) {
		output += fill_contact_output(contactIt->first, contactIt->second);
	}

	list<string> labels = treatyOrStatute.get_labels();
	list<string>::const_iterator labelIt;

	for (labelIt = labels.begin(); labelIt != labels.end(); labelIt++) {
		output += "<mark:label>" + su.esc_xml_markup(*labelIt) + "</mark:label>";
	}

	output += "<mark:goodsAndServices>" +
		su.esc_xml_markup(treatyOrStatute.get_goodsAndServices()) + "</mark:goodsAndServices>"
		"<mark:refNum>" +
		su.esc_xml_markup(treatyOrStatute.get_refNum()) + "</mark:refNum>"
		"<mark:proDate>" +
		su.esc_xml_markup(treatyOrStatute.get_proDate()) + "</mark:proDate>"
		"<mark:title>" + su.esc_xml_markup(treatyOrStatute.get_title()) + "</mark:title>"
		"<mark:execDate>" +
		su.esc_xml_markup(treatyOrStatute.get_execDate()) + "</mark:execDate>"
		"</mark:treatyOrStatute>";

	return output;
}

string fill_court_output(const SMDCourt &court)
{
	StrUtil su;
	string output("");

	output += "<mark:court>"
		"<mark:id>" + su.esc_xml_markup(court.get_id()) + "</mark:id>"
		"<mark:markName>" + su.esc_xml_markup(court.get_markName()) + "</mark:markName>";

	list<pair<SMDHolder::Type::Value, SMDHolder> > holders = court.get_holders();
	list<pair<SMDHolder::Type::Value, SMDHolder> >::const_iterator holderIt;

	for (holderIt = holders.begin(); holderIt != holders.end(); holderIt++) {
		output += fill_holder_output(holderIt->first, holderIt->second);
	}

	map<SMDContact::Type::Value, SMDContact> contacts = court.get_contacts();
	map<SMDContact::Type::Value, SMDContact>::const_iterator contactIt;

	for (contactIt = contacts.begin(); contactIt != contacts.end(); contactIt++) {
		output += fill_contact_output(contactIt->first, contactIt->second);
	}

	list<string> labels = court.get_labels();
	list<string>::const_iterator labelIt;

	for (labelIt = labels.begin(); labelIt != labels.end(); labelIt++) {
		output += "<mark:label>" + su.esc_xml_markup(*labelIt) + "</mark:label>";
	}

	output += "<mark:goodsAndServices>" +
		su.esc_xml_markup(court.get_goodsAndServices()) + "</mark:goodsAndServices>"
		"<mark:refNum>" +
		su.esc_xml_markup(court.get_refNum()) + "</mark:refNum>"
		"<mark:proDate>" +
		su.esc_xml_markup(court.get_proDate()) + "</mark:proDate>"
		"<mark:cc>" + su.esc_xml_markup(court.get_cc()) + "</mark:cc>";

	list<string> regions = court.get_regions();
	list<string>::const_iterator regionIt;

	for (regionIt = regions.begin(); regionIt != regions.end(); regionIt++) {
		output += "<mark:region>" + su.esc_xml_markup(*regionIt) + "</mark:region>";
	}

	output += "<mark:courtName>" + 
		su.esc_xml_markup(court.get_courtName()) + "</mark:courtName>"
		"</mark:court>";

	return output;
}

string fill_mark_output(const SMDMark &mark)
{
	string output("");

	if (mark.is_empty()) {
		return output;
	}

	output += "<mark:mark xmlns:mark=\"urn:ietf:params:xml:ns:mark-1.0\">";

	list<SMDTrademark> trademarks = mark.get_trademarks();
	list<SMDTrademark>::const_iterator trademarkIt;

	for (trademarkIt = trademarks.begin(); 
	     trademarkIt != trademarks.end();
	     trademarkIt++) {
		output += fill_trademark_output(*trademarkIt);
	}

	list<SMDTreatyOrStatute> treatyOrStatutes = mark.get_treatyOrStatutes();
	list<SMDTreatyOrStatute>::const_iterator treatyOrStatuteIt;

	for (treatyOrStatuteIt = treatyOrStatutes.begin();
	     treatyOrStatuteIt != treatyOrStatutes.end();
	     treatyOrStatuteIt++) {
		output += fill_treaty_or_statute_output(*treatyOrStatuteIt);
	}

	list<SMDCourt> courts = mark.get_court();
	list<SMDCourt>::const_iterator courtIt;

	for (courtIt = courts.begin(); courtIt != courts.end(); courtIt++) {
		output += fill_court_output(*courtIt);
	}

	output += "</mark:mark>";
	return output;
}

string fill_smd_output(const SMD &smd)
{
	StrUtil su;
	string output("");

	output += "<smd:signedMark xmlns:smd=\"urn:ietf:params:xml:ns:signedMark-1.0\">"
		"<smd:id>" + su.esc_xml_markup(smd.get_id()) + "</smd:id>";

	SMDIssuerInfo issuerInfo = smd.get_issuerInfo();

	output += "<smd:issuerInfo issuerID=\"" + 
		su.esc_xml_markup(issuerInfo.get_id()) + "\">"
		"<smd:org>" + su.esc_xml_markup(issuerInfo.get_org()) + "</smd:org>"
		"<smd:email>" + su.esc_xml_markup(issuerInfo.get_email()) + "</smd:email>";

	if (!issuerInfo.get_url().empty()) {
		output += "<smd:url>" + su.esc_xml_markup(issuerInfo.get_url()) + "</smd:url>";
	}

	if (!issuerInfo.get_voice().empty()) {
		output += "<smd:voice>" + su.esc_xml_markup(issuerInfo.get_voice()) + "</smd:voice>";
	}

	output += "</smd:issuerInfo>"
		"<smd:notBefore>" + su.esc_xml_markup(smd.get_notBefore()) + "</smd:notBefore>"
		"<smd:notAfter>" + su.esc_xml_markup(smd.get_notAfter()) + "<smd:notAfter>" +
		fill_mark_output(smd.get_mark()) +
		"<Signature xmlns=\"http://www.w3.org/2000/09/xmldsig#\">" + 
		su.esc_xml_markup(smd.get_signature()) + "</Signature>"
		"</smd:signedMark>";
	return output;
}

string fill_launch_output(DomainCreateCmd *cmd)
{
	string output("");

	LaunchCreateCmd launch = cmd->get_launch();
	if (launch.get_phase().get_phase() == LaunchPhase::NONE) {
		return output;
	}

	StrUtil su;

	output += "<launch:create xmlns:launch=\"urn:ietf:params:xml:ns:launch-1.0\"";

	if (launch.get_type() != LaunchCreateCmd::Type::NONE) {
		output += " type=\"" + LaunchCreateCmd::Type::toStr(launch.get_type()) + "\"";
	}

	output += ">"
		"<launch:phase";

	LaunchPhase phase = launch.get_phase();
	if (!phase.get_name().empty()) {
		output += " name=\"" + su.esc_xml_markup(phase.get_name()) + "\"";
	}

	output += ">" + LaunchPhase::toStr(phase.get_phase()) + "</launch:phase>";

	if (!launch.get_notice().is_empty()) {
		ClaimsNotice notice = launch.get_notice();
		output += "<launch:notice>"
			"<launch:noticeID>" + su.esc_xml_markup(notice.get_id()) + "</launch:noticeID>"
			"<launch:notAfter>" + su.esc_xml_markup(notice.get_notAfter()) +
			"</launch:notAfter>"
			"<launch:acceptedDate>" + su.esc_xml_markup(notice.get_acceptedDate()) +
			"</launch:acceptedDate>"
			"</launch:notice>";
	}

	if (!launch.get_codeMarks().empty()) {
		list<CodeMark> codeMarks = launch.get_codeMarks();
		list<CodeMark>::const_iterator it;

		for (it = codeMarks.begin(); it != codeMarks.end(); it++) {
			output += "<launch:codeMark>";

			if (!it->get_code().empty()) {
				output += "<launch:code>" + su.esc_xml_markup(it->get_code()) + "</launch:code>";
			}

			output += fill_mark_output(it->get_mark()) +
				"</launch:codeMark>";
		}

	} else if (!launch.get_signedMarks().empty()) {
		list<SMD> smds = launch.get_signedMarks();
		list<SMD>::const_iterator it;

		for (it = smds.begin(); it != smds.end(); it++) {
			output += fill_smd_output(*it);
		}
		
	} else if (!launch.get_encodedSignedMarks().empty()) {
		list<EncodedSignedMark> encodedSMDs = launch.get_encodedSignedMarks();
		list<EncodedSignedMark>::const_iterator it;

		for (it = encodedSMDs.begin(); it != encodedSMDs.end(); it++) {
			output += "<smd:encodedSignedMark "
				"xmlns:smd=\"urn:ietf:params:xml:ns:signedMark-1.0\"";

			if (!it->get_encoding().empty()) {
				output += " encoding=\"" + su.esc_xml_markup(it->get_encoding()) + "\"";
			}

			output += ">" + su.esc_xml_markup(it->get_data()) + "</smd:encodedSignedMark>";
		}
	}

	output += "</launch:create>";
	return output;
}

void DomainCreate::set_xml_template(const string &xml_template)
{
	StrUtil su;
	Action::set_xml_template_common(xml_template);
	map < string, string, less<string> > to_parse;

	//name
	to_parse["name"] = su.esc_xml_markup(get_command()->get_name());

	//period
	RegistrationPeriod period = get_command()->get_period();
	to_parse["period"] = "";
	if (period.time > 0) {
		to_parse["period"] = "<domain:period unit='" + 
			su.esc_xml_markup(period.unit) + "'>" +
			StrUtil::to_string("%d", period.time) + "</domain:period>";
	}

	//nameservers
	vector<struct NameServer> nss = get_command()->get_nameservers();
	vector<struct NameServer>::const_iterator ns_it;
	set<NSIPAddr>::const_iterator ip_it;
	to_parse["nameservers"] = "";
	if (!nss.empty()) {
		to_parse["nameservers"] = "<domain:ns>";

		for (ns_it = nss.begin(); ns_it != nss.end(); ns_it++) {
			to_parse["nameservers"] += "<domain:hostAttr>";
			to_parse["nameservers"] += "<domain:hostName>" +
				su.esc_xml_markup(ns_it->name) + "</domain:hostName>";
			for (ip_it = ns_it->ips.begin(); ip_it != ns_it->ips.end(); ip_it++) {
				to_parse["nameservers"] += "<domain:hostAddr";
				if ((*ip_it).version != "") {
					to_parse["nameservers"] += " ip='" + 
						su.esc_xml_markup((*ip_it).version) + "'";
				}
				to_parse["nameservers"] += ">" + su.esc_xml_markup((*ip_it).addr) +
					"</domain:hostAddr>";
			}
			to_parse["nameservers"] += "</domain:hostAttr>";
		}

		to_parse["nameservers"] += "</domain:ns>";
	}

	//registrant
	to_parse["registrant"] = "";
	if (get_command()->get_registrant() != "") {
		to_parse["registrant"] = "<domain:registrant>" +
			su.esc_xml_markup(get_command()->get_registrant()) +
			"</domain:registrant>";
	}

	//other contacts
	map< string, string, less<string> > contacts = get_command()->get_contacts();
	map< string, string, less<string> >::const_iterator it_contacts;
	to_parse["other_contacts"] = "";
	for (it_contacts = contacts.begin(); it_contacts != contacts.end();
	     it_contacts++) {
		to_parse["other_contacts"] +=
			"<domain:contact type='" + su.esc_xml_markup((*it_contacts).first) +
			"'>" + su.esc_xml_markup((*it_contacts).second) + "</domain:contact>";
	}

	//auth info
	AuthInfo authInfo;
	authInfo = get_command()->get_authInfo();
	string auth_info_str;
	auth_info_str = "<domain:authInfo><domain:pw>" +
		su.esc_xml_markup(authInfo.get_pw()) + "</domain:pw></domain:authInfo>";
	to_parse["auth_info"] = auth_info_str;

	to_parse["ext_begin"] = "";
	to_parse["ext_end"] = "";
	to_parse["ds_ext"] = "";
	to_parse["launch_ext"] = "";

	if (get_command()->has_extension()) {
		to_parse["ext_begin"] = "<extension>";
		to_parse["ext_end"] = "</extension>";
		to_parse["ds_ext"] = fill_dnssec_output(get_command());
		to_parse["launch_ext"] = fill_launch_output(get_command());
	}
	_xml = StrUtil::parse(_xml, to_parse, "$(", ")$");
}

LIBEPP_NICBR_NS_END
