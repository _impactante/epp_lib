/* 
 * Copyright (C) 2006-2015 Registro.br. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistribution of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY REGISTRO.BR ``AS IS AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIE OF FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL REGISTRO.BR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 */
/* $Id$ */

#include "config.h"

#include "DefRegUpdate.H"
#include "StrUtil.H"

LIBEPP_NICBR_NS_BEGIN

void DefRegUpdate::set_xml_template(const string &xml_template)
{
	StrUtil su;
	Action::set_xml_template_common(xml_template);
	map < string, string, less<string> > to_parse;

	// roid
	to_parse["roid"] = su.esc_xml_markup(get_command()->get_roid());

	// status add
	set<DefRegUpdateCmd::Status> status_add = get_command()->get_status_add();

	to_parse["add"] = "";
	if (!status_add.empty()) {
		to_parse["add"] = "<defReg:add>";

		set<DefRegUpdateCmd::Status>::const_iterator it;
		for (it = status_add.begin(); it != status_add.end(); ++it) {
			to_parse["add"] += "<defReg:status s=\"" + 
				su.esc_xml_markup(it->get_status()) + "\"";
			if (!it->get_lang().empty()) {
				to_parse["add"] += " lang=\"" + su.esc_xml_markup(it->get_lang()) + "\"";
			}
			if (!it->get_msg().empty()) {
				to_parse["add"] += ">" + su.esc_xml_markup(it->get_msg()) + "</defReg:status>";
			} else {
				to_parse["add"] += "/>";
			}
		}

		to_parse["add"] += "</defReg:add>";
	}

	// status rem
	set<DefRegUpdateCmd::Status> status_rem = get_command()->get_status_rem();

	to_parse["rem"] = "";
	if (!status_rem.empty()) {
		to_parse["rem"] = "<defReg:rem>";

		set<DefRegUpdateCmd::Status>::const_iterator it;
		for (it = status_rem.begin(); it != status_rem.end(); ++it) {
			to_parse["rem"] += "<defReg:status s=\"" + 
				su.esc_xml_markup(it->get_status()) + "\"";
			if (!it->get_lang().empty()) {
				to_parse["rem"] += " lang=\"" + su.esc_xml_markup(it->get_lang()) + "\"";
			}
			if (!it->get_msg().empty()) {
				to_parse["rem"] += ">" + su.esc_xml_markup(it->get_msg()) + "</defReg:status>";
			} else {
				to_parse["rem"] += "/>";
			}
		}

		to_parse["rem"] += "</defReg:rem>";
	}

	to_parse["chg"] = "";
	if (!get_command()->get_registrant().empty() ||
	    !get_command()->get_trademark_id().empty() ||
	    !get_command()->get_trademark_country().empty() ||
	    !get_command()->get_trademark_date().empty() ||
	    !get_command()->get_admin_contact().empty() ||
	    !get_command()->get_authInfo().get_pw().empty()) {
		to_parse["chg"] = "<defReg:chg>";

		if (!get_command()->get_registrant().empty()) {
			to_parse["chg"] += "<defReg:registrant>" + 
				su.esc_xml_markup(get_command()->get_registrant()) + "</defReg:registrant>";
		}

		if (!get_command()->get_trademark_id().empty()) {
			to_parse["chg"] += "<defReg:tm>" + 
				su.esc_xml_markup(get_command()->get_trademark_id()) + "</defReg:tm>";
		}

		if (!get_command()->get_trademark_country().empty()) {
			to_parse["chg"] += "<defReg:tmCountry>" + 
				su.esc_xml_markup(get_command()->get_trademark_country()) + "</defReg:tmCountry>";
		}

		if (!get_command()->get_trademark_date().empty()) {
			to_parse["chg"] += "<defReg:tmDate>" + 
				su.esc_xml_markup(get_command()->get_trademark_date()) + "</defReg:tmDate>";
		}

		if (!get_command()->get_admin_contact().empty()) {
			to_parse["chg"] += "<defReg:adminContact>" + 
				su.esc_xml_markup(get_command()->get_admin_contact()) + "</defReg:adminContact>";
		}

		if (!get_command()->get_authInfo().get_pw().empty()) {
			AuthInfo authInfo = get_command()->get_authInfo();

			to_parse["chg"] += "<defReg:authInfo>";
			if (authInfo.get_roid() == "") {
				to_parse["chg"] += "<defReg:pw>" + su.esc_xml_markup(authInfo.get_pw())
					+ "</defReg:pw>";
			} else {
				to_parse["chg"] += "<defReg:pw roid=\"" + 
					su.esc_xml_markup(authInfo.get_roid()) + "\">" + 
					su.esc_xml_markup(authInfo.get_pw()) + "</defReg:pw>";
			}
			to_parse["chg"] += "</defReg:authInfo>";
		}

		to_parse["chg"] += "</defReg:chg>";
	}
  
	_xml = StrUtil::parse(_xml, to_parse, "$(", ")$");
}

LIBEPP_NICBR_NS_END
