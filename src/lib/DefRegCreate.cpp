/* 
 * Copyright (C) 2006-2015 Registro.br. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 1. Redistribution of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY REGISTRO.BR ``AS IS AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIE OF FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL REGISTRO.BR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 */
/* $Id$ */

#include "config.h"

#include "DefRegCreate.H"
#include "StrUtil.H"

LIBEPP_NICBR_NS_BEGIN

void DefRegCreate::set_xml_template(const string &xml_template)
{
	StrUtil su;
	Action::set_xml_template_common(xml_template);
	map < string, string, less<string> > to_parse;

	//name
	DefRegName name = get_command()->get_name();
	to_parse["name_level"] = su.esc_xml_markup(DefRegLevel::toStr(name.get_level()));
	to_parse["name"] = su.esc_xml_markup(name.get_name());
	
	// registrant
	to_parse["registrant"] = su.esc_xml_markup(get_command()->get_registrant());

	// trademark
	to_parse["tm"] = "";
	to_parse["tm_country"] = "";
	to_parse["tm_date"] = "";

	if (!get_command()->get_trademark_id().empty()) {
		to_parse["tm"] = "<defReg:tm>" + 
			su.esc_xml_markup(get_command()->get_trademark_id()) + "</defReg:tm>";
	}
	if (!get_command()->get_trademark_country().empty()) {
		to_parse["tm_country"] = "<defReg:tmCountry>" + 
			su.esc_xml_markup(get_command()->get_trademark_country()) + "</defReg:tmCountry>";
	}
	if (!get_command()->get_trademark_date().empty()) {
		to_parse["tm_date"] = "<defReg:tmDate>" + 
			su.esc_xml_markup(get_command()->get_trademark_date()) + "</defReg:tmDate>";
	}

	// period
	to_parse["period"] = "";
	RegistrationPeriod period = get_command()->get_period();
	if (period.time != 0 && period.unit != "") {
		to_parse["period"] = "<defReg:period unit=\"" +
			su.esc_xml_markup(period.unit) + "\">" + 
			StrUtil::to_string("%d", period.time) + "</defReg:period>";
	}

	// admin contact
	to_parse["admin_contact"] = su.esc_xml_markup(get_command()->get_admin_contact());

	//auth info
	AuthInfo authInfo;
	authInfo = get_command()->get_authInfo();

	string auth_info_str = "";
	if (authInfo.get_pw() != "") {
		auth_info_str = "<defReg:authInfo>";
		if (authInfo.get_roid() == "") {
			auth_info_str += "<defReg:pw>" + su.esc_xml_markup(authInfo.get_pw())
				+ "</defReg:pw>";
		} else {
			auth_info_str += "<defReg:pw roid=\"" + 
				su.esc_xml_markup(authInfo.get_roid()) + "\">" + 
				su.esc_xml_markup(authInfo.get_pw()) + "</defReg:pw>";
		}
		auth_info_str += "</defReg:authInfo>";
	}
	to_parse["auth_info"] = auth_info_str;
  
	_xml = StrUtil::parse(_xml, to_parse, "$(", ")$");
}

LIBEPP_NICBR_NS_END
